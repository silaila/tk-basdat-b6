from django.urls import path
from . import views

app_name = "homepage"

urlpatterns = [
    path('', views.homepage_view, name='myhomepage'),
    path('landingpage/', views.landing_view, name='landingpage'),
]
