from django.shortcuts import render, redirect
from django.db import connection
from django.core.paginator import Paginator
from django.http import HttpResponse

# Create your views here.
def homepage_view(request):
    return render(request, 'homepage.html')

def landing_view(request):
    return render(request, 'landingpage.html')

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        username = request.session.get('username')
        if username is None:
            return redirect('users:mylogin')
        else:
            return function(request, *args, **kwargs)
    return wrapper