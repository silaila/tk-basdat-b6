from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.db import connection

# Create your views here.
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def admin_view(request):
    print ("TEST")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]   
    # with connection.cursor() as cursor:
        print ("TEST00")
        
        cursor.execute("SELECT username "
                    " FROM AKUN_PENGGUNA WHERE username = '{}';".format(username))
        data_profil = cursor.fetchone()
        print ("TEST01")
        print(data_profil)
        args = {
            'data_profil' : data_profil,
            }
        # print ("TEST02")
        return render(request, 'pp-admin-sistem.html', args)
        
def publik_view(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        # username = request.session["username"] 

        cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",
                       [request.session.get("username")])
        user_data = cursor.fetchone()

        # print("user data ya")
        # print(user_data)
        
        cursor.execute("SELECT * FROM PENGGUNA_PUBLIK WHERE username=%s",
                       [request.session.get("username")])
        role_data = cursor.fetchone()               
        # print(role_data)
        args = {
            'user' : user_data,
            'role' : role_data,
            }
        return render(request, 'pp-publik.html', args)

def dokter_view(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        # username = request.session["username"]
        
        cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",
                       [request.session.get("username")])
        user_data = cursor.fetchone()
        
        cursor.execute("SELECT * FROM DOKTER WHERE username=%s",
                       [request.session.get("username")])
        role_data = cursor.fetchone()               
        
        # cursor.execute("SELECT username, nostr, nama, nohp, gelardepan, gelarbelakang "
        #             " FROM DOKTER WHERE username = '{}';".format(username))

        # data_profil = cursor.fetchone()
        # print(data_profil)
        print(role_data)
        args = {
            'user' : user_data,
            'role' : role_data,
            }
        # print ("TEST02")
        return render(request, 'pp-dokter.html', args)

def satgas_view(request):
    print ("TEST")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        
        cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",
                       [request.session.get("username")])
        user_data = cursor.fetchone()
        
        cursor.execute("SELECT a.username, a.idfaskes FROM ADMIN_SATGAS a, "
                        " FASKES F WHERE F.kode = a.idfaskes AND "
                        " a.username=%s",
                        [request.session.get("username")])
        role_data = cursor.fetchone()

        # cursor.execute("SELECT kode FROM FASKES WHERE id_faskes=%s",
        #                [request.session.get("username")])
        # idfaskes_data = cursor.fetchone()                         

        print(role_data)

        args = {
            'user' : user_data,
            'role' : role_data,
            # 'faskes': idfaskes_data,
            }
        return render(request, 'pp-admin-satgas.html', args)

        