from django.urls import path
from . import views

app_name = "profilpengguna"

urlpatterns = [
    # path('', views.profil_view, name='profil'),
    path('profile-admin-sistem/', views.admin_view, name='profiladminsistem'),
    path('profile-pengguna-publik/', views.publik_view, name='profilpenggunapublik'),
    path('profile-dokter/', views.dokter_view, name='profildokter'),
    path('profile-admin-satgas/', views.satgas_view, name='profiladminsatgas'),
]
