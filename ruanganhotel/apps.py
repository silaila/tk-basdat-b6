from django.apps import AppConfig


class RuanganhotelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ruanganhotel'
