from django.urls import path, include
from . import views

app_name = 'ruanganhotel'
urlpatterns = [ 
	
    path('formHotelRoom/', views.formHotelRoom_view, name='formHotelRoom'),
    path('listHotelRoom/', views.listHotelRoom, name='listHotelRoom'),
    path('updateHotelRoom/', views.updateHotelRoom, name='updateHotelRoom'),
    # path('createAppointment/', views.createAppointment, name='createAppointment'),
    # path('deleteAppointment/', views.deleteAppointment, name='deleteAppointment'),


]