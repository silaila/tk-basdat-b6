from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection

def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
        ]

def formHotelRoom_view(request): 
    # with connection.cursor() as cursor:
    #     print('sebelum set siruco')
    #     cursor.execute("SET search_path to SIRUCO;")
    #     # print ('before tes dok')
    #     username = request.session["username"]
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")

        cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",
                        [request.session.get("username")])
        user_data = cursor.fetchone()[0]
        print (user_data)    

        if request.method == 'POST':
            print('masukpost')
            form = BuatHotelRoom(request.POST)
            if form.is_valid():

                kodehotel = request.POST.get('kode')
                koderoom = request.POST.get('koderoom')
                jenisbed = request.POST.get('jenisbed')
                tipe = request.POST.get('tipe')
                harga = request.POST.get('harga')

                

                isi = {'kodehotel': kodehotel, 
                    'koderoom': koderoom, 
                    'jenisbed': jenisbed,
                    'tipe': tipe,
                    'harga': harga}

                    # print(no_str)
                    
                # if kode_kamar is None:
                #     print("masuk none yg no str")
                #     return redirect('ruanganhotel:formHotelRoom')
                #     print('tes room')

                try: 
                    print('tes room 2')
                    #Insert into HOTEL_ROOM 
                    with connection.cursor() as cursor:
                        
                        print('tes insert')
                        cursor.execute("SET search_path TO SIRUCO;")
                        # print(nik, no_str, email, kode, shift, tanggal, rekomendasi)
                        cursor.execute("INSERT INTO HOTEL_ROOM "
                                        "VALUES ('{}', '{}', '{}', '{}', '{}');"
                                        .format(kodehotel, koderoom, jenisbed, tipe, harga))
                        print('tes after insert')
                    
                    return redirect('ruanganhotel:listHotelRoom')

                except: 
                    print('tes room 3')
                    form=BuatHotelRoom(initial=isi)
                    return render(request, 'form-hotelroom.html', {'form': form})
        
                    
        form = BuatHotelRoom()
        args = {
            'user' : user_data,
            'form' : form,
        }
        print("di deket return")
        print (user_data)
        return render(request, 'form-hotelroom.html', args)

        # elif request.method == 'GET':
        #     print('tes room 4')
        #     kodehotel = request.GET.get('kodehotel')
        #     koderoom = request.GET.get('koderoom')
        #     jenisbed = request.GET.get('jenisbed')
        #     tipe = request.GET.get('tipe')
        #     harga = request.GET.get('harga')
        #     # print(username, kode_faskes, shift, tanggal)

        #     isi = {
        #         'kodehotel': kodehotel, 
        #         'koderoom': koderoom, 
        #         'jenisbed': jenisbed,
        #         'tipe': tipe,
        #         'harga': harga} 

        #     print(isi)
        #     if username is None or kodehotel is None:
        #         print("masuk none yg atas")
        #         return redirect('ruanganhotel:formHotelRoom')

        #     if isi is None:
        #         return redirect('ruanganhotel:formHotelRoom')

        #     form=BuatHotelRoom(initial=isi)
        #     return render(request, 'form-hotelroom.html', {'form': form})

def listHotelRoom(request) : 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username)

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.HOTEL_ROOM;")
            data_room = dictfecthall(cursor)
        return render(request, 'list-hotelroom.html', {'room': data_room})


def updateHotelRoom(request):
    with connection.cursor() as cursor:
        print('sebelum set siruco')
        cursor.execute("SET search_path to SIRUCO;")
        print ('before tes dok')
        username = request.session["username"]

        if request.method == 'POST':
            print("masuk post update")
            kodehotel = request.POST.get('kode')
            koderoom = request.POST.get('koderoom')
            jenisbed = request.POST.get('jenisbed')
            tipe = request.POST.get('tipe')
            harga = request.POST.get('harga')

            # isi = {'kodehotel': kodehotel, 
            #         'koderoom': koderoom, 
            #         'jenisbed': jenisbed,
            #         'tipe': tipe,
            #         'harga': harga}
            # print(isi) 

            # with connection.cursor() as cursor:
            #     cursor.execute("SET SEARCH_PATH TO SIRUCO;")
            #     cursor.execute("SELECT koderoom FROM HOTEL_ROOM where koderoom=%s LIMIT 1;", [koderoom])
            #     kode_kamar = cursor.fetchone()[0]
                
            # if kode_kamar is None:
            #     print("masuk none yg no str")
            #     return redirect('ruanganhotel:formHotelRoom')
            # print('tes room')
            
            # print('tes room 2')
            #UPDATE TABLE MEMERIKSA 
            with connection.cursor() as cursor:
                print("masuk update table")
                cursor.execute("SET search_path to SIRUCO;")
                cursor.execute(
                """
                UPDATE HOTEL_ROOM SET jenisbed='{}', tipe='{}', harga='{}'
                WHERE koderoom='{}' AND kodehotel='{}';
                """
                .format(
                jenisbed, 
                tipe, harga, 
                koderoom, kodehotel))
                return redirect('ruanganhotel:listHotelRoom')
        
        elif request.method == 'GET':
            print('tes room 4')
            kode = request.GET.get('kode')
            koderoom = request.GET.get('koderoom')
            print(kode, koderoom)
            # jenisbed = request.GET.get('jenisbed')
            # tipe = request.GET.get('tipe')
            # harga = request.GET.get('harga')

            # isi = {'kodehotel': kodehotel, 
            #         'koderoom': koderoom, 
            #         'jenisbed': jenisbed,
            #         'tipe': tipe,
            #         'harga': harga} 

            # print(isi)

            # if kodehotel is None or koderoom is None:
            #     print("update none banyak")
            #     return redirect('ruanganhotel:listHotelRoom')

            # if isi is None:
            #     print("update isi none")
            #     return redirect('ruanganhotel:listHotelRoom')

            with connection.cursor() as cursor:
                print('ambil data')
                cursor.execute(
                """
                SELECT * FROM SIRUCO.HOTEL_ROOM   
                WHERE koderoom='{}';
                """
                .format(
                koderoom))
                data_hotelroom = dictfecthall(cursor)
                print ('isi data hotel')
                print(data_hotelroom)
                isi = {
                    'kode': kode, 
                    'koderoom': koderoom,
                    'jenisbed': data_hotelroom[0]['jenisbed'],
                    'tipe': data_hotelroom[0]['tipe'],
                    'harga': data_hotelroom[0]['harga']
                } 
            print(isi)
            if koderoom is None or kode is None:
                return redirect('ruanganhotel:listHotelRoom')

            if isi is None:
                return redirect('ruanganhotel:listHotelRoom')

            form=UpdateHotelRoom(initial=isi)
        return render(request, 'update-hotelroom.html', {'form': form})