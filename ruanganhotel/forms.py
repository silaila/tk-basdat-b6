from django import forms
from django.forms import Form
from django.db import connection, transaction, IntegrityError

class BuatHotelRoom(forms.Form):
    LIST_KODE_HOTEL= []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
	        cursor.execute("SELECT kode, kode FROM SIRUCO.HOTEL")
	        self.fields['kode'].choices = cursor.fetchall()
    
    with connection.cursor() as cursor:
        print('mau ambil kode kamar')
        cursor.execute("SELECT koderoom FROM SIRUCO.HOTEL_ROOM ORDER BY koderoom DESC LIMIT 1;")
        kode_kamar_last = cursor.fetchall()[0][0][2:]
        kode_kamar = 'RH' + str((int(kode_kamar_last)+1))
        print(kode_kamar)

    kode = forms.ChoiceField(label='Kode Hotel',  required=True, choices= LIST_KODE_HOTEL, widget=forms.Select(attrs={'class': 'form-control'}))
    koderoom = forms.CharField(label="Kode Ruangan",  initial=kode_kamar, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    jenisbed = forms.CharField(label="Jenis Bed", required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipe = forms.CharField(label="Tipe", required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))   
    harga = forms.CharField(label="Harga per hari", required=True,  widget=forms.TextInput(attrs={'class': 'form-control'}))

class UpdateHotelRoom(forms.Form):
	kode = forms.CharField(label="Kode Hotel", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	koderoom = forms.CharField(label="Kode Ruangan", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	jenisbed = forms.CharField(label="Jenis Bed", required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
	tipe = forms.CharField(label="Tipe", required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))   
	harga = forms.CharField(label="Harga per hari", required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))