--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: siruco; Type: SCHEMA; Schema: -; Owner: db202b06
--

CREATE SCHEMA siruco;


ALTER SCHEMA siruco OWNER TO db202b06;

--
-- Name: jml_pasien_val(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.jml_pasien_val() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
JmlPasienTemp smallint;

BEGIN
SELECT JmlPasien into JmlPasienTemp 
FROM JADWAL_DOKTER J, MEMERIKSA M
WHERE
J.Username = M.Username_Dokter AND 
J.NoSTR = M.NoSTR AND 
J.Shift = M.Praktek_Shift AND 
J.Kode_Faskes = M.Kode_Faskes AND 
J.Tanggal = M.Praktek_Tgl AND
J.Username = New.Username_Dokter AND 
J.NoSTR = New.NoSTR AND 
J.Kode_Faskes = New.Kode_Faskes AND 
J.Shift = New.Praktek_Shift;


IF (JmlPasienTemp >= 30)
THEN
RAISE EXCEPTION 'Tidak bisa membuat perjanjian dengan dokter % (kuota penuh)', New.Username_Dokter;

ELSE
UPDATE JADWAL_DOKTER J
SET JmlPasien = JmlPasien + 1
WHERE 
J.Username = NEW.Username_Dokter AND
J.NoSTR = NEW.NoSTR AND 
J.Kode_Faskes = NEW.Kode_Faskes
AND J.Shift = NEW.Praktek_Shift AND J.Tanggal = NEW.Praktek_Tgl;
END IF;

RETURN NEW;
END;
$$;


ALTER FUNCTION siruco.jml_pasien_val() OWNER TO db202b06;

--
-- Name: jmlbed_func(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.jmlbed_func() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

IF (TG_NAME = 'jmlbed_inc') THEN
UPDATE RUANGAN_RS RS 
SET jmlbed = jmlbed + 1
WHERE RS.KodeRS = NEW.KodeRS AND RS.KodeRuangan = NEW.KodeRuangan;

ELSEIF (TG_NAME = 'jmlbed_dec') THEN
UPDATE RUANGAN_RS RS 
SET jmlbed = jmlbed - 1
WHERE RS.KodeRS = NEW.KodeRS AND RS.KodeRuangan = NEW.KodeRuangan;

RETURN NEW;
END IF;
RETURN NULL;

END;
$$;


ALTER FUNCTION siruco.jmlbed_func() OWNER TO db202b06;

--
-- Name: make_transaksi_rs(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.make_transaksi_rs() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
 IF (TG_OP = 'INSERT') THEN
  INSERT INTO TRANSAKSI_RS VALUES (
  concat ('RS',NEW.KodePasien),
  NEW.KodePasien,
  NULL,
  NULL,
  NEW.TglMasuk,
  ((NEW.TglKeluar::DATE - NEW.TglMasuk::DATE)+1)*500000,
  'Belum Lunas'
  );

  RETURN NEW;

 ELSEIF (TG_OP = 'UPDATE') THEN
  IF (NEW.TglKeluar != OLD.TglKeluar) THEN
   UPDATE TRANSAKSI_RS 
SET TotalBiaya = ((NEW.TglKeluar::DATE - OLD.TglMasuk::DATE)+1)*500000
WHERE KodePasien=NEW.KodePasien;
RETURN NEW;

ELSEIF (NEW.KodePasien != OLD.KodePasien) THEN
 UPDATE TRANSAKSI_RS
SET KodePasien=NEW.KodePasien
 WHERE KodePasien=OLD.KodePasien;
RETURN NEW;
END IF;

 END IF;
END;
$$;


ALTER FUNCTION siruco.make_transaksi_rs() OWNER TO db202b06;

--
-- Name: new_transaksi_booking(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.new_transaksi_booking() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
 IF (TG_OP = 'INSERT') THEN

  INSERT INTO TRANSAKSI_BOOKING VALUES (
  concat (BOOK,NEW.KodePasien),
  NEW.KodePasien,
  NEW.TglMasuk,
  (SELECT New.TotalBayar FROM TRANSAKSI_HOTEL)
  );
  RETURN NEW;


 ELSEIF (TG_OP = 'UPDATE') THEN
  IF (NEW.TglKeluar != OLD.TglKeluar) THEN
   UPDATE TRANSAKSI_BOOKING
SET TotalBayar = (SELECT New.TotalBayar FROM TRANSAKSI_HOTEL)
   WHERE KodePasien=NEW.KodePasien;
RETURN NEW;  
END IF;
 RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION siruco.new_transaksi_booking() OWNER TO db202b06;

--
-- Name: new_transaksi_hotel(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.new_transaksi_hotel() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
 SELECT harga FROM HOTEL_ROOM AS hargaRoom
  WHERE kodeHotel = NEW.kodeHotel 
  AND kodeRoom = NEW.kodeRoom;

 IF (TG_OP = 'INSERT') THEN

  INSERT INTO TRANSAKSI_HOTEL VALUES (
  concat (HOTEL,NEW.KodePasien),
  NEW.KodePasien,
  NULL,
  NULL,
  (NEW.TglKeluar::DATE - NEW.TglMasuk::DATE)*hargaRoom,
  'Belum Lunas'
  );
  RETURN NEW;


 ELSEIF (TG_OP = 'UPDATE') THEN
  IF (NEW.TglKeluar != OLD.TglKeluar) THEN
   UPDATE TRANSAKSI_HOTEL 
SET TotalBayar = (NEW.TglKeluar::DATE - OLD.TglMasuk::DATE) *hargaRoom
   WHERE KodePasien=NEW.KodePasien;
  END IF;


 RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION siruco.new_transaksi_hotel() OWNER TO db202b06;

--
-- Name: new_transaksi_hotel_booking(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.new_transaksi_hotel_booking() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
 hargaRoom integer;
 totalPembayaran integer;

BEGIN
 SELECT harga into hargaRoom FROM HOTEL_ROOM 
  WHERE kodeHotel = NEW.kodeHotel 
  AND kodeRoom = NEW.kodeRoom;

 IF (TG_OP = 'INSERT') THEN

  INSERT INTO TRANSAKSI_HOTEL VALUES (
  concat ('HOTEL',NEW.KodePasien),
  NEW.KodePasien,
  NULL,
  NULL,
  ((NEW.TglKeluar::DATE - NEW.TglMasuk::DATE)+1)*hargaRoom,
  'Belum Lunas'
  );
  
SELECT TotalBayar into totalPembayaran FROM TRANSAKSI_HOTEL 
WHERE KodePasien=NEW.KodePasien;

  INSERT INTO TRANSAKSI_BOOKING VALUES (
  (SELECT IdTransaksi FROM TRANSAKSI_HOTEL WHERE KodePasien=NEW.KodePasien),
  totalPembayaran,
NEW.KodePasien,
  NEW.TglMasuk
  );

  RETURN NEW;


 ELSEIF (TG_OP = 'UPDATE') THEN
  IF (NEW.TglKeluar != OLD.TglKeluar) THEN
   UPDATE TRANSAKSI_HOTEL
SET TotalBayar = ((NEW.TglKeluar::DATE - OLD.TglMasuk::DATE)+1) *hargaRoom
   WHERE KodePasien=NEW.KodePasien;


SELECT TotalBayar into totalPembayaran FROM TRANSAKSI_HOTEL WHERE kodePasien=NEW.kodepasien;

UPDATE TRANSAKSI_BOOKING
SET TotalBayar = totalPembayaran WHERE kodePasien=NEW.kodepasien;
  RETURN NEW;
  END IF;

 RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION siruco.new_transaksi_hotel_booking() OWNER TO db202b06;

--
-- Name: password_pengguna(); Type: FUNCTION; Schema: siruco; Owner: db202b06
--

CREATE FUNCTION siruco.password_pengguna() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN
IF ((New.Password !~ '[0-9]') OR (New.Password !~ '[A-Z]'))
THEN 
RAISE EXCEPTION 'Password % tidak valid. Masukkan password yang terdiri dari minimal satu huruf kapital dan satu digit angka', New.Password;
END IF;
RETURN NEW;
END;

$$;


ALTER FUNCTION siruco.password_pengguna() OWNER TO db202b06;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.admin (
    username character varying(50) NOT NULL
);


ALTER TABLE siruco.admin OWNER TO db202b06;

--
-- Name: admin_satgas; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.admin_satgas (
    username character varying(50) NOT NULL,
    idfaskes character varying(3) NOT NULL
);


ALTER TABLE siruco.admin_satgas OWNER TO db202b06;

--
-- Name: akun_pengguna; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.akun_pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    peran character varying(20) NOT NULL
);


ALTER TABLE siruco.akun_pengguna OWNER TO db202b06;

--
-- Name: bed_rs; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.bed_rs (
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    kodebed character varying(5) NOT NULL
);


ALTER TABLE siruco.bed_rs OWNER TO db202b06;

--
-- Name: daftar_pesan; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.daftar_pesan (
    id_pesanan integer NOT NULL,
    id_transaksi character varying(10) NOT NULL,
    idtransaksimakan character varying(10) NOT NULL,
    kodehotel character varying(5),
    kodepaket character varying(5)
);


ALTER TABLE siruco.daftar_pesan OWNER TO db202b06;

--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE; Schema: siruco; Owner: db202b06
--

CREATE SEQUENCE siruco.daftar_pesan_id_pesanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE siruco.daftar_pesan_id_pesanan_seq OWNER TO db202b06;

--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE OWNED BY; Schema: siruco; Owner: db202b06
--

ALTER SEQUENCE siruco.daftar_pesan_id_pesanan_seq OWNED BY siruco.daftar_pesan.id_pesanan;


--
-- Name: dokter; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.dokter (
    username character varying(50) NOT NULL,
    nostr character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    nohp character varying(12) NOT NULL,
    gelardepan character varying(10) NOT NULL,
    gelarbelakang character varying(10) NOT NULL
);


ALTER TABLE siruco.dokter OWNER TO db202b06;

--
-- Name: faskes; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.faskes (
    kode character varying(3) NOT NULL,
    tipe character varying(30) NOT NULL,
    nama character varying(50) NOT NULL,
    statusmilik character varying(30) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.faskes OWNER TO db202b06;

--
-- Name: gedung; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.gedung (
    kodegedung character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    CONSTRAINT cek_rujukan_gedung CHECK (((isrujukan = '1'::bpchar) OR (isrujukan = '0'::bpchar)))
);


ALTER TABLE siruco.gedung OWNER TO db202b06;

--
-- Name: gejala_pasien; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.gejala_pasien (
    nik character varying(20) NOT NULL,
    namagejala character varying(50) NOT NULL
);


ALTER TABLE siruco.gejala_pasien OWNER TO db202b06;

--
-- Name: hotel; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.hotel (
    kode character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    CONSTRAINT cek_rujukan_hotel CHECK (((isrujukan = '1'::bpchar) OR (isrujukan = '0'::bpchar)))
);


ALTER TABLE siruco.hotel OWNER TO db202b06;

--
-- Name: hotel_room; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.hotel_room (
    kodehotel character varying(5) NOT NULL,
    koderoom character varying(5) NOT NULL,
    jenisbed character varying(10) NOT NULL,
    tipe character varying(10) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.hotel_room OWNER TO db202b06;

--
-- Name: jadwal; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.jadwal (
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE siruco.jadwal OWNER TO db202b06;

--
-- Name: jadwal_dokter; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.jadwal_dokter (
    nostr character varying(20) NOT NULL,
    username character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL,
    jmlpasien integer
);


ALTER TABLE siruco.jadwal_dokter OWNER TO db202b06;

--
-- Name: kamar_rumah; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.kamar_rumah (
    koderumah character varying(5) NOT NULL,
    kodekamar character varying(5) NOT NULL,
    jenisbed character varying(20) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.kamar_rumah OWNER TO db202b06;

--
-- Name: komorbid_pasien; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.komorbid_pasien (
    nik character varying(20) NOT NULL,
    namakomorbid character varying(50) NOT NULL
);


ALTER TABLE siruco.komorbid_pasien OWNER TO db202b06;

--
-- Name: memeriksa; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.memeriksa (
    nik_pasien character varying(20) NOT NULL,
    nostr character varying(20) NOT NULL,
    username_dokter character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    praktek_shift character varying(15) NOT NULL,
    praktek_tgl date NOT NULL,
    rekomendasi character varying(500)
);


ALTER TABLE siruco.memeriksa OWNER TO db202b06;

--
-- Name: paket_makan; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.paket_makan (
    kodehotel character varying(5) NOT NULL,
    kodepaket character varying(5) NOT NULL,
    nama character varying(20) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.paket_makan OWNER TO db202b06;

--
-- Name: pasien; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.pasien (
    nik character varying(20) NOT NULL,
    idpendaftar character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    ktp_jalan character varying(30) NOT NULL,
    ktp_jalan character varying(30) NOT NULL,
    ktp_kecamatan character varying(30) NOT NULL,
    ktp_kabkot character varying(30) NOT NULL,
    ktp_prov character varying(30) NOT NULL,
    dom_jalan character varying(30) NOT NULL,
    dom_kelurahan character varying(30) NOT NULL,
    dom_kecamatan character varying(30) NOT NULL,
    dom_kabkot character varying(30) NOT NULL,
    dom_prov character varying(30) NOT NULL,
    notelp character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL
);


ALTER TABLE siruco.pasien OWNER TO db202b06;

--
-- Name: pengguna_publik; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.pengguna_publik (
    username character varying(50) NOT NULL,
    nik character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    status character varying(10) NOT NULL,
    peran character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL,
    CONSTRAINT cek_status_val CHECK ((((status)::text = 'AKTIF'::text) OR ((status)::text = 'TDK AKTIF'::text)))
);


ALTER TABLE siruco.pengguna_publik OWNER TO db202b06;

--
-- Name: reservasi_gedung; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.reservasi_gedung (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodegedung character varying(5),
    noruang character varying(5),
    notempattidur character varying(5)
);


ALTER TABLE siruco.reservasi_gedung OWNER TO db202b06;

--
-- Name: reservasi_hotel; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.reservasi_hotel (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodehotel character varying(5) NOT NULL,
    koderoom character varying(5) NOT NULL
);


ALTER TABLE siruco.reservasi_hotel OWNER TO db202b06;

--
-- Name: reservasi_rs; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.reservasi_rs (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    kodebed character varying(5) NOT NULL,
    kodeventilator character varying(5) NOT NULL
);


ALTER TABLE siruco.reservasi_rs OWNER TO db202b06;

--
-- Name: reservasi_rumah; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.reservasi_rumah (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koderumah character varying(5),
    kodekamar character varying(5)
);


ALTER TABLE siruco.reservasi_rumah OWNER TO db202b06;

--
-- Name: ruangan_gedung; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.ruangan_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    namaruangan character varying(20) NOT NULL
);


ALTER TABLE siruco.ruangan_gedung OWNER TO db202b06;

--
-- Name: ruangan_rs; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.ruangan_rs (
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    tipe character varying(10) NOT NULL,
    jmlbed integer NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.ruangan_rs OWNER TO db202b06;

--
-- Name: rumah; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.rumah (
    koderumah character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL,
    CONSTRAINT cek_rujukan_rumah CHECK (((isrujukan = '1'::bpchar) OR (isrujukan = '0'::bpchar)))
);


ALTER TABLE siruco.rumah OWNER TO db202b06;

--
-- Name: rumah_sakit; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.rumah_sakit (
    kode_faskes character varying(3) NOT NULL,
    isrujukan character(1) NOT NULL,
    CONSTRAINT cek_rujukan_rumah_sakit CHECK (((isrujukan = '1'::bpchar) OR (isrujukan = '0'::bpchar)))
);


ALTER TABLE siruco.rumah_sakit OWNER TO db202b06;

--
-- Name: telepon_faskes; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.telepon_faskes (
    kode_faskes character varying(3) NOT NULL,
    notelp character varying(20) NOT NULL
);


ALTER TABLE siruco.telepon_faskes OWNER TO db202b06;

--
-- Name: tempat_tidur_gedung; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.tempat_tidur_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    notempattidur character varying(5) NOT NULL
);


ALTER TABLE siruco.tempat_tidur_gedung OWNER TO db202b06;

--
-- Name: tes; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.tes (
    nik_pasien character varying(20) NOT NULL,
    tanggaltes date NOT NULL,
    jenis character varying(10) NOT NULL,
    status character varying(15) NOT NULL,
    nilaict character varying(5),
    CONSTRAINT cek_jenis_val CHECK ((((jenis)::text = 'PCR'::text) OR ((jenis)::text = 'Antigen'::text))),
    CONSTRAINT cek_status_tes_val CHECK ((((status)::text = 'Reaktif'::text) OR ((status)::text = 'Non-Reaktif'::text)))
);


ALTER TABLE siruco.tes OWNER TO db202b06;

--
-- Name: transaksi_booking; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.transaksi_booking (
    idtransaksibooking character varying(10) NOT NULL,
    totalbayar integer,
    kodepasien character varying(20),
    tglmasuk date
);


ALTER TABLE siruco.transaksi_booking OWNER TO db202b06;

--
-- Name: transaksi_hotel; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.transaksi_hotel (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL
);


ALTER TABLE siruco.transaksi_hotel OWNER TO db202b06;

--
-- Name: transaksi_makan; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.transaksi_makan (
    idtransaksi character varying(10) NOT NULL,
    idtransaksimakan character varying(10) NOT NULL,
    totalbayar integer
);


ALTER TABLE siruco.transaksi_makan OWNER TO db202b06;

--
-- Name: transaksi_rs; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.transaksi_rs (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20) NOT NULL,
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    tglmasuk date NOT NULL,
    totalbiaya integer,
    statusbayar character varying(15) NOT NULL
);


ALTER TABLE siruco.transaksi_rs OWNER TO db202b06;

--
-- Name: transaksi_rumah; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.transaksi_rumah (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL,
    tglmasuk date
);


ALTER TABLE siruco.transaksi_rumah OWNER TO db202b06;

--
-- Name: ventilator; Type: TABLE; Schema: siruco; Owner: db202b06
--

CREATE TABLE siruco.ventilator (
    koders character varying(3) NOT NULL,
    kodeventilator character varying(5) NOT NULL,
    kondisi character varying(10) NOT NULL
);


ALTER TABLE siruco.ventilator OWNER TO db202b06;

--
-- Name: daftar_pesan id_pesanan; Type: DEFAULT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.daftar_pesan ALTER COLUMN id_pesanan SET DEFAULT nextval('siruco.daftar_pesan_id_pesanan_seq'::regclass);


--
-- Data for Name: admin; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.admin (username) FROM stdin;
agung
boni
cinta
dira
emma
fani
ghina
hani
ian
juki
kemal
lena
maisha
nadin
omar
pevita
qira
raisa
tiara
umar
\.


--
-- Data for Name: admin_satgas; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.admin_satgas (username, idfaskes) FROM stdin;
nadin	120
pevita	190
qira	101
raisa	145
tiara	132
umar	120
ian	190
fani	132
hani	190
juki	145
\.


--
-- Data for Name: akun_pengguna; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.akun_pengguna (username, password, peran) FROM stdin;
agung	Gnuga1	calon pasien
boni	Inob2	penanggung jawab
cinta	Atnic3	calon pasien
dira	Arid4	calon pasien
emma	Amme5	calon pasien
fani	Inaf6	calon pasien
ghina	Anihg7	calon pasien
hani	Inah8	calon pasien
ian	Nai9	penanggung jawab
juki	Ikuj10	penanggung jawab
kemal	Lamek11	penanggung jawab
lena	Anel12	penanggung jawab
maisha	Ahsiam13	calon pasien
nadin	Nidan14	penanggung jawab
omar	Ramo15	penanggung jawab
pevita	Ativep16	penanggung jawab
qira	Ariq17	calon pasien
raisa	Asiar18	penanggung jawab
tiara	Arait19	calon pasien
umar	Ramu20	calon pasien
vania	Ainav21	penanggung jawab
wiena	Aneiw22	penanggung jawab
xavier	Reivax23	calon pasien
yoanna 	Annaoy24	calon pasien
zahra	Arhaz25	penanggung jawab
amira	Arima33	penanggung jawab
ayra	Arya93	calon pasien
jamilah	Jamilah01	penanggung jawab
lisa	Lisa21	penanggung jawab
rose	Rose99	calon pasien
tiffany	Tiffany23	penanggung jawab
\.


--
-- Data for Name: bed_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.bed_rs (koders, koderuangan, kodebed) FROM stdin;
145	SL17	B099
132	DS69	BD09
120	R90	BED10
120	R90	BED19
190	RR35	BSB1
190	RR35	BSB6
132	V86	BD12
145	SL17	B060
\.


--
-- Data for Name: daftar_pesan; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.daftar_pesan (id_pesanan, id_transaksi, idtransaksimakan, kodehotel, kodepaket) FROM stdin;
6	HOTEL59990	TM0099	IH30	P02
7	HOTEL60005	TM0150	MH63	P04
8	HOTEL60003	TM0398	BW39	P05
9	HOTEL59991	TM1178	MH63	P07
10	HOTEL59999	TM2288	JWM98	P03
\.


--
-- Data for Name: dokter; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.dokter (username, nostr, nama, nohp, gelardepan, gelarbelakang) FROM stdin;
boni	13456789	Boni Sitohang	8912345678	Prof. dr.	Sp.P(K)
maisha	20394857	Maisha Annisa	8561122334	Prof. dr.	Sp.P(K)
omar	34567891	Omar Daulay	89933445512	dr.	Sp.P(K)
lena	23456780	Lenna Astari	8123456094	Prof. dr.	Sp.P(K)
cinta	12345678	Cinta Sukma	85790909090	dr.	Sp.P(K)
\.


--
-- Data for Name: faskes; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.faskes (kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
101	tingkat 1	Puskesmas Bojongsari	Pemerintah 	Jl. Bojongsari	Kebon Kelapa	Gambir	Jakarta Pusat	DKI Jakarta
120	tingkat 2	RSUD Yogyakarta	Pemerintah 	Jl. Kenangan	Tridaya Indah	Ngampilan	Yogyakarta	DIY
132	tingkat 3	RS Sehat Bersama	Swasta	Jl. Jalan	Gandus	Gandus	Palembang	Sumatera Selatan
145	tingkat 3	RS Siloa Kemenangan 	Swasta	Jl. Kemenangan	Maju Sari	Kuta	Denpasar	Bali
190	tingkat 3	RSUPN Kesehatan Utama	Pemerintah 	Jl. Kesehatan	Wiyung	Wiyung	Surabaya	Jawa Timur
139	tingkat 1	RS MITRA	Swasta	Jl. Margonda	Pancoran Mas	Beji	Depok	Jawa Barat
168	tingkat 3	RS Sukasehat	Swasta	Jl. Melati Raya	Keongmas	Gedong Air	Lampung	Sumatera Selatan
\.


--
-- Data for Name: gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.gedung (kodegedung, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
GM	Gama	0	Jl. Bogerejo	Sale	Kaliori	Tunggulsari	DIY 
TS	Treasury	1	Jl. Jatiadi	Gunem	Sumber	Sekarsari	Kalimatan Barat
WM	Wisma 46	1	Jl. Krikilan	Pamotan	Bulu	Pelemsari	Nusa Tenggara Barat
AST	Astra	0	Jl. Ngadem	Sedan	Rembang	Tlogotunggal	Jawa Tengah
RFL	Raffles	0	Jl. Seren	Pamotan	Sulang	Pedak	Jawa Timur
\.


--
-- Data for Name: gejala_pasien; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.gejala_pasien (nik, namagejala) FROM stdin;
59990	batuk, pilek, mudah lelah, pusing
59991	batuk, pilek
59994	batuk, tidak bisa mencium, pusing
59995	tidak bisa mencium, pilek
59998	tidak bisa merasa, batuk, mudah lelah, pusing
59999	mudah lelah, pusing, pilek
60001	pilek, batuk, tidak bisa mencium, demam 
60003	tidak bisa mencium
60004	demam, sesak nafas, tidak bisa merasa
60005	demam, sesak nafas, pilek
\.


--
-- Data for Name: hotel; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.hotel (kode, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
HS15	Hotel Santika	1	Jl. Santika	Mulia Jaya	Leuwiliang	Bogor	Jawa Barat
IH30	Ibis Hotel	0	Jl. Ibis	Jaya Suti	Ogan Ilir	Medan	Sumatera Utara
JWM98	Hotel J.W. Marriot	1	Jl. Marriot	Jaya Abadi	Kalibata	Jakarta 	DKI Jakarta
MH63	Margo Hotel 	1	Jl. Margo 	Beji	Pancoran Mas	Depok	Jawa Barat
BW39	Hotel Bumi Wiyata	0	Jl. Wiyata 	Panglima Polim	Sukmajaya	Surabaya	Jawa Timur
\.


--
-- Data for Name: hotel_room; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.hotel_room (kodehotel, koderoom, jenisbed, tipe, harga) FROM stdin;
HS15	HR12	Single	deluxe	500000
IH30	IR90	Twin	premium	800000
JWM98	JR90	King	suite	1000000
MH63	MR04	King	suite	950000
BW39	BR70	Twin	deluxe	400000
HS15	HR73	Twin	deluxe	450000
MH63	MR91	Single	deluxe	510000
JWM98	JR04	Single	premium	740000
IH30	IR50	King	suite	900000
BW39	BR01	Twin	premium	750000
\.


--
-- Data for Name: jadwal; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.jadwal (kode_faskes, shift, tanggal) FROM stdin;
120	Malam	2020-09-29
190	Malam	2020-09-29
101	Pagi	2020-09-30
145	Siang	2020-09-30
132	Malam	2020-09-30
120	Malam	2020-09-30
190	Malam	2020-10-01
132	Malam	2020-10-01
190	Pagi	2020-10-02
145	Malam	2020-10-02
\.


--
-- Data for Name: jadwal_dokter; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.jadwal_dokter (nostr, username, kode_faskes, shift, tanggal, jmlpasien) FROM stdin;
13456789	boni	190	Pagi	2020-10-02	30
13456789	boni	190	Malam	2020-10-01	39
34567891	omar	132	Malam	2020-09-30	11
12345678	cinta	120	Malam	2020-09-30	28
20394857	maisha	145	Siang	2020-09-30	30
12345678	cinta	120	Malam	2020-09-29	23
23456780	lena	101	Pagi	2020-09-30	23
\.


--
-- Data for Name: kamar_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.kamar_rumah (koderumah, kodekamar, jenisbed, harga) FROM stdin;
GDG	GDG08	Twin	150000
KJG	KJG00	Twin	160000
LEK	LEK07	King	250000
LIM	LIM72	Single	165000
NOW	NOW19	Double	180000
BDU	BDU62	Single	170000
LEK	LEK01	King	230000
BDU	BDU63	Double	190000
LIM	LIM70	Double	198000
KJG	KJG82	Single	195000
\.


--
-- Data for Name: komorbid_pasien; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.komorbid_pasien (nik, namakomorbid) FROM stdin;
59990	hipertensi
59991	diabetes
59994	diabetes, asma
59995	autoimon
59998	pneumonia, asma
59999	jantung koroner
60001	TBC
60003	hipertensi
60004	hipertensi, diabetes
60005	asma
\.


--
-- Data for Name: memeriksa; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.memeriksa (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl, rekomendasi) FROM stdin;
59990	12345678	cinta	120	Malam	2020-09-29	pasien memiliki komorbid yang dapat memperparah kondisinya. Oleh karenanya Pasien perlu dirujuk ke RS besar. 
59994	12345678	cinta	120	Malam	2020-09-29	Pasien memiliki penyakit penyerta berupa jantung. Sehingga, dibutuhkan bantuan dokter jantung untuk merawat pasien. 
59995	20394857	maisha	145	Siang	2020-09-30	Pasien perlu meminum obat dengan dosis yang lumayan tinggi karena memiliki riwayat penyakit penyerta yang memungkinkan untuk sulit sembuh jika hanya dengan dosis yang biasa. 
59998	34567891	omar	132	Malam	2020-09-30	Pasien disarankan untuk dirujuk ke rumah sakit terdekat yang menyediakan ventilator karena pasien mengalami keluhan sesak nafas. 
59999	12345678	cinta	120	Malam	2020-09-30	Pasien diharapkan untuk melakukan isolasi mandiri karena kondisinya yang dikatakan tidak terlalu parah (nilai ct diatas 30). 
60003	23456780	lena	101	Pagi	2020-09-30	Pasien sudah dapat dikatakan tidak memiliki gejala yang berat lagi. Sehingga dapat dilakukan tes di minggu depan. Namun, pasien tetap diharapkan menjaga asupan, terutama minum air putih yang banyak. 
60004	20394857	maisha	145	Siang	2020-09-30	Pasien membutuhkan ventilator, serta pasien memiliki riwayat alergi kacang yang cukup parah. Pihak rumah sakit perlu memperhatikan makanan pada pasien. 
59991	12345678	cinta	120	Malam	2020-09-29	pasien memiliki gejala yang tidak terlalu parah. Belum lagi, pasien masih sangat muda dan dapat dipastikan akan cepat pulih. Oleh karenanya, pasien diharapkan untuk melakukan tes kembali dua hari lagi. 
60001	23456780	lena	101	Pagi	2020-09-30	Pasien diharapkan untuk isolasi mandiri, namun tidak di rumahnya karena ada banyak sanak saudara sehingga dikhawatirkan menjadi cluster penyebaran covid yang baru. Disarankan untuk isoman di gedung/hotel. 
60005	23456780	lena	101	Pagi	2020-09-30	Pasien memiliki penyakit penyerta TBC, sehingga perlu dilakukan rontgen terlebih dahulu untuk melihat apakah TBC nya berpengaruh akibat COVID-19. Dan apabila dilihat dalam 2 hari masa pengobatan, pasien dirasa kondisinya melemah, maka pasien perlu dalam pengawasan khusus oleh 2 dokter paru, dan dipasang ventilator. 
\.


--
-- Data for Name: paket_makan; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.paket_makan (kodehotel, kodepaket, nama, harga) FROM stdin;
HS15	P01	Ayam Geprek	25000
IH30	P02	Nasi Liwet	20000
JWM98	P03	Nasi Goreng	22000
MH63	P04	Nasi Kebuli	30000
BW39	P05	Nasi Uduk 	18000
JWM98	P06	Salad Buah	19000
MH63	P07	Spagheti 	25000
\.


--
-- Data for Name: pasien; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.pasien (nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp) FROM stdin;
59990	amira	Amira Rachman	jalan apel	Tridaya Sakti	Tambun Selatan	Bekasi	Jawa Barat	jalan apel	Tridaya Sakti	Tambun Selatan	Bekasi	Jawa Barat	2188330310	81523939981
59991	ayra	Ayra Zhafira	jalan delima	Mekarsari	Tangerang Selatan	Tangerang	Banten	jalan kelapa	Kaliarang	Kaliurang	Yogyakarta	DIY Yogyakarta	27433377888	87888800022
59994	jamilah	Jamilah Mulan	jalan mangga	Sukamaju	Beji	Depok	Jawa Barat	jalan mangga	Sukamaju	Beji	Depok	Jawa Barat	2177665544	85233344455
59995	lisa	Lisa Ayuma	jalan durian	Sukakamu	Beji Timur	Jakarta	DKI Jakarta	jalan durian	Sukakamu	Beji Timur	Jakarta	DKI Jakarta	2198765432	82199933777
59998	rose	Rose Mawar	jalan nanas	Sukasukaaku	Bojongsari	Cibinong	Jawa Barat	jalan ceri	Pondok Cina	Pancoran Mas	Depok	Jawa Barat	2167892344	85212345678
59999	tiffany	Tiffany Lee	jalan kedondong	Mangun Jawa	Kukusan	Denpasar	Bali	jalan kedondong	Mangun Jawa	Kukusan	Denpasar	Bali	36192837465	85166448833
60001	vania	Vania Rebecca	jalan pisang	Harapan Jaya	Pancoran Mas	Depok	Jawa Barat	jalan pisang	Harapan Jaya	Pancoran Mas	Depok	Jawa Barat	2100998877	85710335789
60003	xavier	Xavier Johnson	jalan semangka	Pondok Kodnop	Bedahan	Yogyakarta	DIY Yogyakarta	jalan semangka	Pondok Kodnop	Bedahan	Yogyakarta	DIY Yogyakarta	27455667788	88866779911
60004	yoanna 	Yoanna Alexandra	jalan anggur	Lokasaji	Tapos 	Palembang	Sumatera Selatan	jalan mengkudu	Duren Sawit	Duren Sawit	Jakarta	DKI Jakarta	2154879988	89988776622
60005	zahra	Zahra Azzahra	jalan leci	Palama	Pasir Putih	Surabaya	Jawa Timur	jalan leci	Palama	Pasir Putih	Surabaya	Jawa Timur	3198765432	87812345789
\.


--
-- Data for Name: pengguna_publik; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.pengguna_publik (username, nik, nama, status, peran, nohp) FROM stdin;
amira	59990	Amira Rachman	AKTIF	penanggung jawab	81523939981
ayra	59991	Ayra Zhafira	TDK AKTIF	calon pasien	87888800022
jamilah	59994	Jamilah Mulan	AKTIF	penanggung jawab	85233344455
lisa	59995	Lisa Ayuma	TDK AKTIF	penanggung jawab	82199933777
rose	59998	Rose Mawar	AKTIF	calon pasien	85212345678
tiffany	59999	Tiffany Lee	TDK AKTIF	penanggung jawab	85166448833
vania	60001	Vania Rebecca	AKTIF	penanggung jawab	85710335789
wiena	60002	Wiena Cantika	TDK AKTIF	penanggung jawab	81234567890
xavier	60003	Xavier Johnson	TDK AKTIF	calon pasien	88866779911
yoanna 	60004	Yoanna Alexandra	AKTIF	calon pasien	89988776622
zahra	60005	Zahra Azzahra	AKTIF	penanggung jawab	87812345789
\.


--
-- Data for Name: reservasi_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.reservasi_gedung (kodepasien, tglmasuk, tglkeluar, kodegedung, noruang, notempattidur) FROM stdin;
59990	2020-06-10	2020-12-10	GM	GM01	2
59991	2020-04-18	2020-04-23	GM	GM02	1
59994	2020-06-16	2020-06-20	GM	GM02	2
59995	2020-04-20	2020-04-25	TS	TS01	1
59998	2020-07-21	2020-07-28	TS	TS01	2
59999	2020-05-15	2020-05-21	TS	TS02	1
60001	2020-06-23	2020-06-30	WM	WM47	2
60003	2020-06-23	2020-06-30	AST	AST01	1
60004	2020-04-19	2020-04-23	AST	AST02	1
60005	2020-05-19	2020-05-21	RFL	RFL01	1
\.


--
-- Data for Name: reservasi_hotel; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.reservasi_hotel (kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom) FROM stdin;
59990	2020-10-01	2020-10-07	HS15	HR12
60005	2020-10-02	2020-10-05	IH30	IR90
60003	2020-10-02	2020-10-05	JWM98	JR90
59991	2020-10-02	2020-10-07	MH63	MR91
59999	2020-10-03	2020-10-05	IH30	IR50
59994	2020-10-01	2020-10-05	BW39	BR70
\.


--
-- Data for Name: reservasi_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.reservasi_rs (kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed, kodeventilator) FROM stdin;
59994	2020-09-29	2020-10-05	190	RR35	BSB6	VR690
59995	2020-09-30	2020-10-07	132	DS69	BD09	VR301
59998	2020-09-30	2020-10-07	120	R90	BED19	VR298
59999	2020-09-30	2020-10-07	145	SL17	B060	VR451
60001	2020-09-30	2020-10-08	190	RR35	BSB1	VR690
59990	2020-09-30	2020-10-08	132	DS69	BD09	VR301
\.


--
-- Data for Name: reservasi_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.reservasi_rumah (kodepasien, tglmasuk, tglkeluar, koderumah, kodekamar) FROM stdin;
59991	2020-04-10	2020-04-17	LEK	LEK01
59994	2020-05-10	2020-05-16	LEK	LEK07
60001	2020-06-10	2020-06-17	NOW	NOW19
60004	2020-04-11	2020-04-18	BDU	BDU62
60005	2020-05-11	2020-05-17	GDG	GDG08
60003	2020-06-11	2020-06-18	KJG	KJG82
59998	2020-07-11	2020-07-18	LIM	LIM72
\.


--
-- Data for Name: ruangan_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.ruangan_gedung (kodegedung, koderuangan, namaruangan) FROM stdin;
GM	GM01	Mawar
TS	TS01	Melati
WM	WM46	Kenanga
AST	AST01	Asoka
RFL	RFL01	Lily
TS	TS02	Alamanda
WM	WM47	Anggrek
AST	AST02	Adenium
AST	AST03	Akasia
GM	GM02	Bougenville
\.


--
-- Data for Name: ruangan_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.ruangan_rs (koders, koderuangan, tipe, jmlbed, harga) FROM stdin;
132	V86	Kelas 1	11	800000
120	R90	VIP	6	1000000
145	SL17	Kelas 2	16	700000
190	RR35	VVIP	3	2000000
132	DS69	Kelas 3	24	400000
\.


--
-- Data for Name: rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.rumah (koderumah, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
GDG	Gadang	1	Jl. Ahmad Yani	Bojonggede	Wonocolo	Nagan Raya	Jawa Timur
KJG	Kajang	1	Jl. Wonokromo	Jonggol	Wonokromo	Pidie	Jawa Tengah
LEK	Leko	1	Jl. Mastrip	Kotawisata	Karangpilang	Asahan	Jawa Barat
LIM	Limas	0	Jl. Gunungsari	Mangun	Dukuh Pakis	Dairi	Sumatra Barat
NOW	Nowou	0	Jl. Menganti	Mekarsari	Wiyung	Deli Serdang	Gorontalo
BDU	Badui	1	Jl. Jambangan	Linggi	Jambangan	Karo	Kalimatan Selatan
\.


--
-- Data for Name: rumah_sakit; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.rumah_sakit (kode_faskes, isrujukan) FROM stdin;
101	1
120	1
132	1
145	0
190	0
\.


--
-- Data for Name: telepon_faskes; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.telepon_faskes (kode_faskes, notelp) FROM stdin;
101	2199911122
120	2188796543
132	2177554429
139	2137648591
145	2188331199
168	2183584935
190	7152834058
\.


--
-- Data for Name: tempat_tidur_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.tempat_tidur_gedung (kodegedung, koderuangan, notempattidur) FROM stdin;
GM	GM01	1
GM	GM01	2
GM	GM02	1
GM	GM02	2
GM	GM02	3
TS	TS01	1
TS	TS01	2
TS	TS02	1
TS	TS02	2
WM	WM46	1
WM	WM47	2
AST	AST01	1
AST	AST02	1
AST	AST03	1
RFL	RFL01	1
\.


--
-- Data for Name: tes; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.tes (nik_pasien, tanggaltes, jenis, status, nilaict) FROM stdin;
59990	2020-09-23	PCR	Reaktif	32
59991	2020-09-23	Antigen	Non-Reaktif	27
59994	2020-09-23	Antigen	Non-Reaktif	25
59995	2020-09-23	PCR	Reaktif	30
59998	2020-09-24	PCR	Reaktif	28
59999	2020-09-24	PCR	Non-Reaktif	31
60001	2020-09-25	Antigen	Non-Reaktif	30
60003	2020-09-25	Antigen	Reaktif	25
60004	2020-09-25	PCR	Reaktif	17
60005	2020-09-26	PCR	Reaktif	29
\.


--
-- Data for Name: transaksi_booking; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.transaksi_booking (idtransaksibooking, totalbayar, kodepasien, tglmasuk) FROM stdin;
HOTEL59990	3500000	59990	2020-10-01
HOTEL60005	3200000	60005	2020-10-02
HOTEL60003	4000000	60003	2020-10-02
HOTEL59991	3060000	59991	2020-10-02
HOTEL59999	2700000	59999	2020-10-03
HOTEL59994	2000000	59994	2020-10-01
\.


--
-- Data for Name: transaksi_hotel; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.transaksi_hotel (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar) FROM stdin;
HOTEL59990	59990	\N	\N	3500000	Belum Lunas
HOTEL60005	60005	\N	\N	3200000	Belum Lunas
HOTEL60003	60003	\N	\N	4000000	Belum Lunas
HOTEL59991	59991	\N	\N	3060000	Belum Lunas
HOTEL59999	59999	\N	\N	2700000	Belum Lunas
HOTEL59994	59994	\N	\N	2000000	Belum Lunas
\.


--
-- Data for Name: transaksi_makan; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.transaksi_makan (idtransaksi, idtransaksimakan, totalbayar) FROM stdin;
HOTEL59990	TM0099	125000
HOTEL60005	TM0150	140000
HOTEL60003	TM0398	88000
HOTEL59991	TM1178	125000
HOTEL59999	TM2288	40000
\.


--
-- Data for Name: transaksi_rs; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.transaksi_rs (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, tglmasuk, totalbiaya, statusbayar) FROM stdin;
RS59994	59994	\N	\N	2020-09-29	3500000	Belum Lunas
RS59995	59995	\N	\N	2020-09-30	4000000	Belum Lunas
RS59998	59998	\N	\N	2020-09-30	4000000	Belum Lunas
RS59999	59999	\N	\N	2020-09-30	4000000	Belum Lunas
RS60001	60001	\N	\N	2020-09-30	4500000	Belum Lunas
RS59990	59990	\N	\N	2020-09-30	4500000	Belum Lunas
\.


--
-- Data for Name: transaksi_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.transaksi_rumah (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar, tglmasuk) FROM stdin;
TR07	59991	2020-04-17	2020-04-17 03:45:00	230000	berhasil	\N
TR14	59994	2020-05-16	2020-05-16 07:47:00	250000	berhasil	\N
TR26	60001	2020-06-17	2020-06-17 09:07:00	180000	berhasil	\N
TR33	60004	2020-04-18	2020-04-18 19:31:00	170000	berhasil	\N
TR42	60005	2020-05-17	2020-05-17 03:30:00	150000	berhasil	\N
TR57	60003	2020-06-18	2020-06-18 15:44:00	195000	sedang diproses	\N
TR68	59998	2020-07-18	2020-07-18 13:20:00	165000	sedang diproses	\N
\.


--
-- Data for Name: ventilator; Type: TABLE DATA; Schema: siruco; Owner: db202b06
--

COPY siruco.ventilator (koders, kodeventilator, kondisi) FROM stdin;
101	VR290	berfungsi
120	VR298	rusak
132	VR301	rusak
145	VR451	berfungsi
190	VR690	berfungsi
\.


--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE SET; Schema: siruco; Owner: db202b06
--

SELECT pg_catalog.setval('siruco.daftar_pesan_id_pesanan_seq', 10, true);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (username);


--
-- Name: admin_satgas admin_satgas_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_pkey PRIMARY KEY (username);


--
-- Name: akun_pengguna akun_pengguna_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.akun_pengguna
    ADD CONSTRAINT akun_pengguna_pkey PRIMARY KEY (username);


--
-- Name: bed_rs bed_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_pkey PRIMARY KEY (koders, koderuangan, kodebed);


--
-- Name: daftar_pesan daftar_pesan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_pkey PRIMARY KEY (id_pesanan, id_transaksi, idtransaksimakan);


--
-- Name: dokter dokter_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.dokter
    ADD CONSTRAINT dokter_pkey PRIMARY KEY (username, nostr);


--
-- Name: faskes faskes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.faskes
    ADD CONSTRAINT faskes_pkey PRIMARY KEY (kode);


--
-- Name: gedung gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.gedung
    ADD CONSTRAINT gedung_pkey PRIMARY KEY (kodegedung);


--
-- Name: gejala_pasien gejala_pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.gejala_pasien
    ADD CONSTRAINT gejala_pasien_pkey PRIMARY KEY (nik, namagejala);


--
-- Name: hotel hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.hotel
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (kode);


--
-- Name: hotel_room hotel_room_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.hotel_room
    ADD CONSTRAINT hotel_room_pkey PRIMARY KEY (kodehotel, koderoom);


--
-- Name: jadwal_dokter jadwal_dokter_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_pkey PRIMARY KEY (nostr, username, kode_faskes, shift, tanggal);


--
-- Name: jadwal jadwal_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.jadwal
    ADD CONSTRAINT jadwal_pkey PRIMARY KEY (kode_faskes, shift, tanggal);


--
-- Name: kamar_rumah kamar_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.kamar_rumah
    ADD CONSTRAINT kamar_rumah_pkey PRIMARY KEY (koderumah, kodekamar);


--
-- Name: komorbid_pasien komorbid_pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.komorbid_pasien
    ADD CONSTRAINT komorbid_pasien_pkey PRIMARY KEY (nik, namakomorbid);


--
-- Name: memeriksa memeriksa_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_pkey PRIMARY KEY (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl);


--
-- Name: paket_makan paket_makan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.paket_makan
    ADD CONSTRAINT paket_makan_pkey PRIMARY KEY (kodehotel, kodepaket);


--
-- Name: pasien pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.pasien
    ADD CONSTRAINT pasien_pkey PRIMARY KEY (nik);


--
-- Name: pengguna_publik pengguna_publik_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.pengguna_publik
    ADD CONSTRAINT pengguna_publik_pkey PRIMARY KEY (username);


--
-- Name: reservasi_gedung reservasi_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_hotel reservasi_hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_rs reservasi_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_rumah reservasi_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: ruangan_gedung ruangan_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.ruangan_gedung
    ADD CONSTRAINT ruangan_gedung_pkey PRIMARY KEY (kodegedung, koderuangan);


--
-- Name: ruangan_rs ruangan_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.ruangan_rs
    ADD CONSTRAINT ruangan_rs_pkey PRIMARY KEY (koders, koderuangan);


--
-- Name: rumah rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.rumah
    ADD CONSTRAINT rumah_pkey PRIMARY KEY (koderumah);


--
-- Name: rumah_sakit rumah_sakit_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.rumah_sakit
    ADD CONSTRAINT rumah_sakit_pkey PRIMARY KEY (kode_faskes);


--
-- Name: telepon_faskes telepon_faskes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.telepon_faskes
    ADD CONSTRAINT telepon_faskes_pkey PRIMARY KEY (kode_faskes, notelp);


--
-- Name: tempat_tidur_gedung tempat_tidur_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.tempat_tidur_gedung
    ADD CONSTRAINT tempat_tidur_gedung_pkey PRIMARY KEY (kodegedung, koderuangan, notempattidur);


--
-- Name: tes tes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.tes
    ADD CONSTRAINT tes_pkey PRIMARY KEY (nik_pasien, tanggaltes);


--
-- Name: transaksi_booking transaksi_booking_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_pkey PRIMARY KEY (idtransaksibooking);


--
-- Name: transaksi_hotel transaksi_hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_hotel
    ADD CONSTRAINT transaksi_hotel_pkey PRIMARY KEY (idtransaksi);


--
-- Name: transaksi_makan transaksi_makan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_makan
    ADD CONSTRAINT transaksi_makan_pkey PRIMARY KEY (idtransaksi, idtransaksimakan);


--
-- Name: transaksi_rs transaksi_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_rs
    ADD CONSTRAINT transaksi_rs_pkey PRIMARY KEY (idtransaksi);


--
-- Name: transaksi_rumah transaksi_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_rumah
    ADD CONSTRAINT transaksi_rumah_pkey PRIMARY KEY (idtransaksi);


--
-- Name: ventilator ventilator_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.ventilator
    ADD CONSTRAINT ventilator_pkey PRIMARY KEY (koders, kodeventilator);


--
-- Name: memeriksa cek_jml_pasien; Type: TRIGGER; Schema: siruco; Owner: db202b06
--

CREATE TRIGGER cek_jml_pasien BEFORE INSERT OR UPDATE OF nik_pasien ON siruco.memeriksa FOR EACH ROW EXECUTE PROCEDURE siruco.jml_pasien_val();


--
-- Name: akun_pengguna cek_password; Type: TRIGGER; Schema: siruco; Owner: db202b06
--

CREATE TRIGGER cek_password BEFORE INSERT OR UPDATE OF password ON siruco.akun_pengguna FOR EACH ROW EXECUTE PROCEDURE siruco.password_pengguna();


--
-- Name: reservasi_rs jmlbed_dec; Type: TRIGGER; Schema: siruco; Owner: db202b06
--

CREATE TRIGGER jmlbed_dec AFTER INSERT ON siruco.reservasi_rs FOR EACH ROW EXECUTE PROCEDURE siruco.jmlbed_func();


--
-- Name: bed_rs jmlbed_inc; Type: TRIGGER; Schema: siruco; Owner: db202b06
--

CREATE TRIGGER jmlbed_inc AFTER INSERT ON siruco.bed_rs FOR EACH ROW EXECUTE PROCEDURE siruco.jmlbed_func();


--
-- Name: reservasi_rs new_rujukan_rs; Type: TRIGGER; Schema: siruco; Owner: db202b06
--

CREATE TRIGGER new_rujukan_rs AFTER INSERT OR UPDATE ON siruco.reservasi_rs FOR EACH ROW EXECUTE PROCEDURE siruco.make_transaksi_rs();


--
-- Name: reservasi_hotel transaksi_hotel_booking; Type: TRIGGER; Schema: siruco; Owner: db202b06
--

CREATE TRIGGER transaksi_hotel_booking AFTER INSERT OR UPDATE ON siruco.reservasi_hotel FOR EACH ROW EXECUTE PROCEDURE siruco.new_transaksi_hotel_booking();


--
-- Name: admin_satgas admin_satgas_idfaskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_idfaskes_fkey FOREIGN KEY (idfaskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin_satgas admin_satgas_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_username_fkey FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin admin_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.admin
    ADD CONSTRAINT admin_username_fkey FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bed_rs bed_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_koders_fkey FOREIGN KEY (koders, koderuangan) REFERENCES siruco.ruangan_rs(koders, koderuangan);


--
-- Name: bed_rs bed_rs_koders_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_koders_fkey1 FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes);


--
-- Name: daftar_pesan daftar_pesan_id_transaksi_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_id_transaksi_fkey FOREIGN KEY (id_transaksi, idtransaksimakan) REFERENCES siruco.transaksi_makan(idtransaksi, idtransaksimakan);


--
-- Name: daftar_pesan daftar_pesan_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_kodehotel_fkey FOREIGN KEY (kodehotel, kodepaket) REFERENCES siruco.paket_makan(kodehotel, kodepaket);


--
-- Name: dokter dokter_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.dokter
    ADD CONSTRAINT dokter_username_fkey FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: gejala_pasien gejala_pasien_nik_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.gejala_pasien
    ADD CONSTRAINT gejala_pasien_nik_fkey FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: hotel_room hotel_room_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.hotel_room
    ADD CONSTRAINT hotel_room_kodehotel_fkey FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode);


--
-- Name: jadwal_dokter jadwal_dokter_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_kode_faskes_fkey FOREIGN KEY (kode_faskes, shift, tanggal) REFERENCES siruco.jadwal(kode_faskes, shift, tanggal);


--
-- Name: jadwal_dokter jadwal_dokter_nostr_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_nostr_fkey FOREIGN KEY (nostr, username) REFERENCES siruco.dokter(nostr, username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal jadwal_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.jadwal
    ADD CONSTRAINT jadwal_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kamar_rumah kamar_rumah_koderumah_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.kamar_rumah
    ADD CONSTRAINT kamar_rumah_koderumah_fkey FOREIGN KEY (koderumah) REFERENCES siruco.rumah(koderumah);


--
-- Name: komorbid_pasien komorbid_pasien_nik_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.komorbid_pasien
    ADD CONSTRAINT komorbid_pasien_nik_fkey FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: memeriksa memeriksa_nik_pasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_nik_pasien_fkey FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik);


--
-- Name: memeriksa memeriksa_nostr_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_nostr_fkey FOREIGN KEY (nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl) REFERENCES siruco.jadwal_dokter(nostr, username, kode_faskes, shift, tanggal);


--
-- Name: paket_makan paket_makan_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.paket_makan
    ADD CONSTRAINT paket_makan_kodehotel_fkey FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode);


--
-- Name: pasien pasien_idpendaftar_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.pasien
    ADD CONSTRAINT pasien_idpendaftar_fkey FOREIGN KEY (idpendaftar) REFERENCES siruco.pengguna_publik(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengguna_publik pengguna_publik_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.pengguna_publik
    ADD CONSTRAINT pengguna_publik_username_fkey FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_gedung reservasi_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_kodegedung_fkey FOREIGN KEY (kodegedung, noruang, notempattidur) REFERENCES siruco.tempat_tidur_gedung(kodegedung, koderuangan, notempattidur);


--
-- Name: reservasi_gedung reservasi_gedung_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik);


--
-- Name: reservasi_hotel reservasi_hotel_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_kodehotel_fkey FOREIGN KEY (kodehotel, koderoom) REFERENCES siruco.hotel_room(kodehotel, koderoom);


--
-- Name: reservasi_hotel reservasi_hotel_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik);


--
-- Name: reservasi_rs reservasi_rs_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik);


--
-- Name: reservasi_rs reservasi_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes);


--
-- Name: reservasi_rs reservasi_rs_koders_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey1 FOREIGN KEY (koders, koderuangan) REFERENCES siruco.ruangan_rs(koders, koderuangan);


--
-- Name: reservasi_rs reservasi_rs_koders_fkey2; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey2 FOREIGN KEY (koders, koderuangan, kodebed) REFERENCES siruco.bed_rs(koders, koderuangan, kodebed);


--
-- Name: reservasi_rs reservasi_rs_koders_fkey3; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey3 FOREIGN KEY (koders, kodeventilator) REFERENCES siruco.ventilator(koders, kodeventilator);


--
-- Name: reservasi_rumah reservasi_rumah_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik);


--
-- Name: reservasi_rumah reservasi_rumah_koderumah_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_koderumah_fkey FOREIGN KEY (koderumah, kodekamar) REFERENCES siruco.kamar_rumah(koderumah, kodekamar);


--
-- Name: ruangan_gedung ruangan_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.ruangan_gedung
    ADD CONSTRAINT ruangan_gedung_kodegedung_fkey FOREIGN KEY (kodegedung) REFERENCES siruco.gedung(kodegedung);


--
-- Name: ruangan_rs ruangan_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.ruangan_rs
    ADD CONSTRAINT ruangan_rs_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes);


--
-- Name: rumah_sakit rumah_sakit_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.rumah_sakit
    ADD CONSTRAINT rumah_sakit_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode);


--
-- Name: telepon_faskes telepon_faskes_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.telepon_faskes
    ADD CONSTRAINT telepon_faskes_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tempat_tidur_gedung tempat_tidur_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.tempat_tidur_gedung
    ADD CONSTRAINT tempat_tidur_gedung_kodegedung_fkey FOREIGN KEY (kodegedung, koderuangan) REFERENCES siruco.ruangan_gedung(kodegedung, koderuangan);


--
-- Name: tes tes_nik_pasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.tes
    ADD CONSTRAINT tes_nik_pasien_fkey FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_idtransaksibooking_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_idtransaksibooking_fkey FOREIGN KEY (idtransaksibooking) REFERENCES siruco.transaksi_hotel(idtransaksi);


--
-- Name: transaksi_booking transaksi_booking_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik);


--
-- Name: transaksi_booking transaksi_booking_kodepasien_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_kodepasien_fkey1 FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_hotel(kodepasien, tglmasuk);


--
-- Name: transaksi_hotel transaksi_hotel_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_hotel
    ADD CONSTRAINT transaksi_hotel_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik);


--
-- Name: transaksi_makan transaksi_makan_idtransaksi_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_makan
    ADD CONSTRAINT transaksi_makan_idtransaksi_fkey FOREIGN KEY (idtransaksi) REFERENCES siruco.transaksi_hotel(idtransaksi);


--
-- Name: transaksi_rs transaksi_rs_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_rs
    ADD CONSTRAINT transaksi_rs_kodepasien_fkey FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rs(kodepasien, tglmasuk);


--
-- Name: transaksi_rumah transaksi_rumah_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.transaksi_rumah
    ADD CONSTRAINT transaksi_rumah_kodepasien_fkey FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rumah(kodepasien, tglmasuk);


--
-- Name: ventilator ventilator_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202b06
--

ALTER TABLE ONLY siruco.ventilator
    ADD CONSTRAINT ventilator_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes);


--
-- PostgreSQL database dump complete
--

