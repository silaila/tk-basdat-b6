from django import forms
from django.forms import Form

class UpdateTransaksiHotel(forms.Form):
	idtransaksi = forms.CharField(label="Id Transaksi", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	kodepasien = forms.CharField(label="Kode Pasien", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	tanggalpembayaran = forms.CharField(label="TanggalPembayaran", required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))
	waktupembayaran = forms.CharField(label="Waktu Pembayaran", required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))   
	totalbayar = forms.CharField(label="Total Bayar", required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))
	statusbayar = forms.CharField(label="Status Bayar", required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))