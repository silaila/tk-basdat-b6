from django.urls import path, include
from . import views

app_name = 'transaksihotel'
urlpatterns = [ 
    path('listTransaksiHotel/', views.listTransaksiHotel, name='listTransaksiHotel'),
    path('updateTransaksiHotel/', views.updateTransaksiHotel, name='updateTransaksiHotel'),
]