from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection

def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
        ]
        
def listTransaksiHotel(request) : 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username)

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_HOTEL;")
            data_transhotel = dictfecthall(cursor)
        return render(request, 'list-transaksihotel.html', {'transhotel': data_transhotel})

def updateTransaksiHotel(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

        if request.method == 'POST':
            print("masuk post update")
            idtransaksi = request.POST.get('idtransaksi')
            kodepasien = request.POST.get('kodepasien')
            tanggalpembayaran = request.POST.get('tanggalpembayaran')
            waktupembayaran = request.POST.get('waktupembayaran')
            totalbayar = request.POST.get('totalbayar')
            statusbayar = request.POST.get('statusbayar')

            with connection.cursor() as cursor:
                print("masuk update table")
                cursor.execute("SET search_path to SIRUCO;")
                cursor.execute(
                """
                UPDATE TRANSAKSI_HOTEL SET statusbayar='{}'
                WHERE idtransaksi='{}';
                """
                .format(statusbayar, idtransaksi
                ))
                return redirect('transaksihotel:listTransaksiHotel')
        
        elif request.method == 'GET':
            print('tes room 4')
            idtransaksi = request.GET.get('idtransaksi')
            kodepasien = request.GET.get('kodepasien')
            tanggalpembayaran = request.GET.get('tanggalpembayaran')
            waktupembayaran = request.GET.get('waktupembayaran')
            totalbayar = request.GET.get('totalbayar')

            with connection.cursor() as cursor:
                print('ambil data')
                cursor.execute(
                """
                SELECT * FROM SIRUCO.TRANSAKSI_HOTEL   
                WHERE idtransaksi='{}';
                """
                .format(idtransaksi))

                data_transhotel = dictfecthall(cursor)
                print ('isi data hotel')
                print(data_transhotel)
                isi = {
                    'idtransaksi': idtransaksi, 
                    'kodepasien': kodepasien,
                    'tanggalpembayaran': tanggalpembayaran,
                    'waktupembayaran': waktupembayaran,
                    'totalbayar': totalbayar,
                    'statusbayar': data_transhotel[0]['statusbayar']
                } 
            print(isi)
            if idtransaksi is None:
                return redirect('transaksihotel:listTransaksiHotel')

            if isi is None:
                return redirect('transaksihotel:listTransaksiHotel')

            form=UpdateTransaksiHotel(initial=isi)
        return render(request, 'update-transaksihotel.html', {'form': form})