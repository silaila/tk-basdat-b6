from django.urls import path
from . import views

app_name = "users"

urlpatterns = [
    path('login/', views.user_login, name='mylogin'),
    path('register/adminsistem/', views.admin_sistem_view, name='regadminsistem'),
    path('register/penggunapublik/', views.pengguna_publik_form_view, name='regpenggunappublik'),
    path('register/dokter/', views.dokter_form_view, name='regdokter'),
    path('register/adminsatgas/', views.admin_satgas_form_view, name='regadminsatgas'),
    path('logout/', views.userLogout, name='logout'),
]
