def add_variable_to_context(request):
    return {
        'username': request.session.get('username'),
        'role': request.session.get('role')
    }
