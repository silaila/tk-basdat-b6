from django import forms
from django.db import connection
from django.core.paginator import Paginator
# from django.core.validator import RegexValidator

class LoginForm(forms.Form):
    username = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
#----------------ADMIN SISTEM----------------#
class RegAdminForm(forms.Form):
    username = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

#----------------PENGGUNA PUBLIK----------------#
class RegPenggunaPublikForm(forms.Form):
    username = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    nama = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class':'form-control'}))
    nik = forms.CharField(label='NIK', widget=forms.TextInput(attrs={'class':'form-control'}))
    nohp = forms.CharField(label='No. Hp', widget=forms.TextInput(attrs={'class':'form-control'}))

#----------------PENGGUNA DOKTER----------------#
class RegDokterForm(forms.Form):
    username = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    nostr = forms.CharField(label='No. STR', widget=forms.TextInput(attrs={'class':'form-control'}))
    nama = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class':'form-control'}))
    nohp = forms.CharField(label='nohp', widget=forms.TextInput(attrs={'class':'form-control'}))
    gelardepan = forms.CharField(label='Gelar Depan', widget=forms.TextInput(attrs={'class':'form-control'}))
    gelarbelakang = forms.CharField(label='Gelar Belakang', widget=forms.TextInput(attrs={'class':'form-control'}))

#----------------ADMIN SATGAS----------------#
class RegAdminSatgasForm(forms.Form):
    KODE_FASKES_CHOICES = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT Kode, Kode FROM SIRUCO.FASKES")
            self.fields['kode_faskes'].choices = cursor.fetchall()
    
    kode_faskes = forms.ChoiceField(label='Kode Faskes', required=True, choices= KODE_FASKES_CHOICES, widget=forms.Select(attrs={
        'class': 'form-control',
		'id' : 'kode_faskes',
		'required' : True}))
    username = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    