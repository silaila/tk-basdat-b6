from django.contrib.sessions.models import Session
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from .forms import LoginForm
from .forms import RegAdminForm, RegDokterForm, RegAdminSatgasForm, RegPenggunaPublikForm
from django.db import connection
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.db import connection
from django.urls import reverse
# Create your views here.
def fetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
        ]
    
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
    
def home(request):
    if request.session.is_empty():
        return render(request, 'homepage:myhomepage')
    else:
        return render(request, 'homepage:landingpage')

def getrole(role):
    return user_login.roleNow

#----------------LOGIN----------------#
def user_login(request):
    roleNow = 'None'
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]

                cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE " + "username = %s AND password = %s;", [username, password])

                print("USERNAME: " + username)
                print("PASSWORD: " + password)
                
                user = fetchall(cursor)
                print("TEST: 1")
                print(user)
                
                if len(user) != 0:
                    print("SIAPA YA? masuk user!=0")
                    user = user[0]
                    print(user) #berhasil sampai sini kak:)
                    username = user["username"]
                    request.session["username"] =  username
                    # request.session["nama_lengkap"] = user["nama_lengkap"]
                    print("TEST: 3")

                    request.session.modified = True
                    if check_role(username, "ADMIN"):
                        print("SIAPA YA? dalem checkrole ")
                        if check_role(username, "ADMIN_SATGAS"):
                            request.session["role"] = "ADMIN_SATGAS"
                        elif check_role(username, "DOKTER"):
                            print("masuk dokter")
                            request.session["role"] = "DOKTER"
                        else :
                            request.session["role"] = "ADMIN"
                        print(user)
                    # elif check_role(username, "ADMIN_SATGAS"):
                    #     request.session["role"] = "ADMIN_SATGAS"
                    elif check_role(username, "PENGGUNA_PUBLIK"):
                        request.session["role"] = "PENGGUNA_PUBLIK"

                    print("CEK DISINI")
                    roleNow = request.session["role"]
                    print(roleNow)
                    if roleNow == "ADMIN":
                        print("ADMIN SIAP DI RETRIEVE")
                        return redirect('profilpengguna:profiladminsistem')
                        print("CEK 0")
                    elif roleNow == "ADMIN_SATGAS":
                        print("CEK 1")
                        return redirect('profilpengguna:profiladminsatgas')
                    elif roleNow == "DOKTER":
                        return redirect('profilpengguna:profildokter')
                    elif roleNow == "PENGGUNA_PUBLIK":
                        return redirect('profilpengguna:profilpenggunapublik')
                print("CEK 2")
                messages.error(request, 'Your email and/or password do not match')
                return redirect('users:mylogin')
        else:
            form = LoginForm()
            return render(request, "login.html", {'form':form})

def check_role(username, role):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO SIRUCO;")
        query = "SELECT * FROM " + role + " WHERE username = %s;"
        cursor.execute(query, [username])
        row = fetchall(cursor)

        if len(row) != 0:
            return True
        else:
            return False

#--------LOGOUT----------#
def userLogout(request):
    request.session.flush()
    return redirect('homepage:myhomepage')

#----------------REGISTER ADMIN SISTEM----------------#
def admin_sistem_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegAdminForm(request.POST)
            if form.is_valid():
                try: 
                    username = request.POST.get('username')
                    print(username)
                    password = request.POST.get('password')
                    peran= 'admin'

                    #Insert into AKUN_PENGGUNA 
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO AKUN_PENGGUNA(username, password, peran)"
                                    "VALUES ('{}', '{}', '{}');".format(username,password,peran))

                    # INSERT TO ADMIN_SISTEM
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO ADMIN(username)"
                                    "VALUES ('{}');".format(username))

                    request.session['username'] = username
                    request.session["role"] = "ADMIN"
                    return redirect('profilpengguna:profiladminsistem')
                
                except: 
                    return render(request, 'admin_sistem_form.html', 
                    {'form': form, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})


    form = RegAdminForm()
    args = {
        'form' : form,
    }
    return render(request, 'admin_sistem_form.html', args)

#----------------REGISTER PENGGUNA_PUBLIK----------------#
def pengguna_publik_form_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegPenggunaPublikForm(request.POST)
            if form.is_valid():
                try:
                    username = request.POST.get('username')
                    password = request.POST.get('password')
                    nama = request.POST.get('nama')
                    nik = request.POST.get('nik')
                    nohp = request.POST.get('nohp')
                    status = 'AKTIF'
                    
                    peran='PENGGUNA_PUBLIK'
                    #Insert into AKUN_PENGGUNA 
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO AKUN_PENGGUNA(username, password, peran)"
                                    "VALUES ('{}', '{}', '{}');".format(username,password,peran))

                    #Insert into PENGGUNA_PUBLIK
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO PENGGUNA_PUBLIK(username, nik, nama, status, peran, nohp)"
                                    "VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(username, nik, nama, status, peran, nohp))

                    request.session['username'] = username
                    request.session["role"] = "PENGGUNA_PUBLIK"
                    return redirect('profilpengguna:profilpenggunapublik')
                
                except:
                    return render(request, 'pengguna_publik_form.html', 
                    {'form': form, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})
            
    form = RegPenggunaPublikForm()
    args = {
        'form' : form,
    }
    return render(request, 'pengguna_publik_form.html', args)

#----------------REGISTER DOKTER ----------------#
def dokter_form_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegDokterForm(request.POST)
            if form.is_valid():
                try:
                    username = request.POST.get('username')
                    password = request.POST.get('password')
                    nostr = request.POST.get('nostr')
                    nama = request.POST.get('nama')
                    nohp = request.POST.get('nohp')
                    gelardepan = request.POST.get('gelardepan')
                    gelarbelakang = request.POST.get('gelarbelakang')

                    peran='dokter'
                    #Insert into AKUN_PENGGUNA 
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO AKUN_PENGGUNA(username, password, peran)"
                                    "VALUES ('{}', '{}', '{}');".format(username,password,peran))

                    # INSERT TO ADMIN_SISTEM
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO ADMIN(username)"
                                    "VALUES ('{}');".format(username))

                    #Insert into DOKTER
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO DOKTER(username, nostr, nama, nohp, gelardepan, gelarbelakang)"
                                    "VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(username, nostr, nama, nohp, gelardepan, gelarbelakang))

                    request.session['username'] = username
                    request.session["role"] = "DOKTER"
                    return redirect('profilpengguna:profildokter')
                
                except:
                    return render(request, 'dokter_form.html', 
                    {'form': form, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})
    form = RegDokterForm()
    args = {
        'form' : form,
    }
    return render(request, 'dokter_form.html', args)

#---------- REGISTRASI ADMIN SATGAS -----------#
def admin_satgas_form_view(request):
    print ("masuk satgassss")
    with connection.cursor() as cursor:
        print ("akan masuk satgas")
        if request.method == 'POST':
            print ("masuk satgassss2")
            form = RegAdminSatgasForm(request.POST)
            if form.is_valid():
                try: 
                    print ("form is valid")
                    username = request.POST.get('username')
                    password = request.POST.get('password')
                    idfaskes = request.POST.get('kode_faskes')
                    peran = 'Admin Satgas'
                    print ("ID FASKES")
                    print (idfaskes)
                    #Insert into AKUN_PENGGUNA 
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO AKUN_PENGGUNA(username, password, peran)"
                                    "VALUES ('{}', '{}', '{}');".format(username,password,peran))

                    print("isiii")
        
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO ADMIN(username)"
                                    "VALUES ('{}');".format(username))
                    
                    print("abis insert ke admin")
                    
                    #Insert into ADMIN SATGAS
                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO ADMIN_SATGAS(username, idfaskes)"
                                    "VALUES ('{}', '{}');".format(username, idfaskes))
                    
                    print("isiii satgas")

                    request.session['username'] = username
                    request.session["role"] = "ADMIN_SATGAS"
                    return redirect('profilpengguna:profiladminsatgas')
                
                except: 
                    return render(request, 'admin_satgas_form.html', 
                    {'form': form, 'message': 'Password harus terdiri dari minimal 1 huruf kapital dan 1 angka.'})

                

    form = RegAdminSatgasForm()
    args = {
        'form' : form,
    }
    return render(request, 'admin_satgas_form.html', args)
