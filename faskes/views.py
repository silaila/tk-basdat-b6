from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
from .forms import reservasiFaskes, UpdateFaskes, reservasiJadwalFaskes
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection

def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
    ]

def ReservasiFaskes(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"] 
        role = request.session.get('role') 

        formbaru = reservasiFaskes()


        if request.method == 'POST':
            form = reservasiFaskes(request.POST)
            if form.is_valid():
                kode = request.POST.get('kode')
                tipe = request.POST.get('tipe')
                nama = request.POST.get('nama')
                statusmilik = request.POST.get('statusmilik')
                jalan = request.POST.get('jalan')
                kelurahan = request.POST.get('kelurahan')
                kecamatan = request.POST.get('kecamatan')
                kabkot = request.POST.get('kabkot')
                prov = request.POST.get('prov')

                cursor.execute("SET search_path TO SIRUCO;")
                cursor.execute("INSERT INTO FASKES "
                                "VALUES ('{}', '{}', '{}', '{}', '{}','{}','{}','{}','{}');"
                                .format(kode,tipe,nama,statusmilik,jalan,kelurahan,kecamatan,kabkot,prov))

                return redirect('faskes:listReservasiFaskes')

        print("masuk sini")
        return render(request, 'reservasi-faskes.html', {'form': formbaru})


def listReservasiFaskes(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"] 
        role = request.session.get('role') 

        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.FASKES")
            list_faskes = dictfecthall(cursor)
        return render(request, 'list-faskes.html', {'listreservasi': list_faskes})

        # else :
        #     with connection.cursor() as cursor:
        #         cursor.execute("SELECT * FROM SIRUCO.FASKES, SIRUCO.FASKES")
        #         list_reservasi = dictfecthall(cursor)
        #     return render(request, 'list-faskes.html', {'listreservasi': list_faskes})

def deleteFaskes(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]
    
        if request.method == 'GET':
            kode = request.GET.get('kode')
            tipe = request.GET.get('tipe')

        
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM SIRUCO.FASKES WHERE kode='{}';".format(kode))
    
    return redirect('faskes:listReservasiFaskes')

        

def updateFaskes(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

        if request.method == 'POST':
            form = UpdateFaskes(request.POST)
            if form.is_valid():
                kode = request.POST.get('kode')
                tipe = request.POST.get('tipe')
                nama = request.POST.get('nama')
                statusmilik = request.POST.get('statusmilik')
                jalan = request.POST.get('jalan')
                kelurahan = request.POST.get('kelurahan')
                kecamatan = request.POST.get('kecamatan')
                kabkot = request.POST.get('kabkot')
                prov = request.POST.get('prov')
                
                with connection.cursor() as cursor:
                    cursor.execute("SET search_path to SIRUCO;")
                    cursor.execute(
                    """
                    UPDATE FASKES SET tipe= '{}',
                    nama= '{}',
                    statusmilik= '{}',
                    jalan= '{}',
                    kelurahan= '{}',
                    kecamatan= '{}',
                    kabkot= '{}',
                    prov= '{}'
                    WHERE kode='{}';
                    """
                    .format(tipe,nama,statusmilik,jalan,kelurahan,kecamatan,kabkot,prov,kode))
                
                return redirect('faskes:listReservasiFaskes')

        
        elif request.method == 'GET':
            kode = request.GET.get('kode')
            
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.FASKES WHERE kode='{}';".format(kode))
                list_faskes = dictfecthall(cursor)[0]

                isi = {'kode': list_faskes["kode"], 
                    'tipe': list_faskes["tipe"], 
                    'nama': list_faskes["nama"],
                    'statusmilik': list_faskes["statusmilik"],
                    'jalan': list_faskes["jalan"],
                    'kelurahan': list_faskes["kelurahan"],
                    'kecamatan': list_faskes["kecamatan"],
                    'kabkot': list_faskes["kabkot"],
                    'prov': list_faskes["prov"],
                }

    

            # if isi is None:
            #     print("update isi none")
            #     return redirect('dokter:listAppointment')

                form=UpdateFaskes(initial=isi)
            return render(request, 'update-Faskes.html', {'form': form}) 


def detailFaskes(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

    if request.method == 'GET':
        kode = request.GET.get('kode')
        tipe = request.GET.get('tipe')

    
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.FASKES WHERE kode='{}';".format(kode))
        data_faskes = dictfecthall(cursor)[0]
        print (data_faskes)

    return render(request, 'detail-faskes.html', {'data_faskes' : data_faskes})

def listJadwalFaskes(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username)

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.JADWAL;")
        jadwal = dictfecthall(cursor)
    return render(request, 'list-jadwalfaskes.html', {'jadwal': jadwal})

def buatJadwalFaskes(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

    formbaru = reservasiJadwalFaskes()
    if request.method == 'POST':
        form = reservasiJadwalFaskes(request.POST)
        if form.is_valid():
            kode = request.POST.get('kode')
            shift = request.POST.get('shift')
            tanggal = request.POST.get('tanggal')

            with connection.cursor() as cursor:
                cursor.execute("SET search_path TO SIRUCO;")
                cursor.execute(
                    """
                    INSERT INTO Jadwal 
                    VALUES ('{}','{}','{}');
                    """
                    .format(kode,shift,tanggal))

            return redirect('faskes:listJadwalFaskes')

    return render(request, 'reservasi-jadwalfaskes.html', {'form': formbaru})