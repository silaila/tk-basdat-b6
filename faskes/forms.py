from django import forms
from django.forms import Form, ModelForm
from django.db import connection, transaction, IntegrityError


class reservasiFaskes(forms.Form):
    LIST_NIK = []
    LIST_Tipe = []
    LIST_Status = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT DISTINCT tipe, tipe FROM SIRUCO.FASKES")
            self.fields['tipe'].choices = cursor.fetchall()

            cursor.execute("SELECT DISTINCT statusmilik, statusmilik FROM SIRUCO.FASKES")
            self.fields['statusmilik'].choices = cursor.fetchall()
            # cursor.execute("SELECT Kode_Faskes, Kode_Faskes FROM SIRUCO.RUMAH_SAKIT")
            # self.fields['kode_rs'].choices = cursor.fetchall()
    with connection.cursor() as cursor:
        cursor.execute("SELECT MAX(kode) FROM SIRUCO.FASKES;")
        kode =  cursor.fetchall()[0][0]
        print("CEK KODE")

        kodebaru = str((int(kode)+1))
        print (kodebaru)

    kode = forms.CharField(label='Kode Faskes',  initial= kodebaru, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    tipe = forms.ChoiceField(label="Tipe", choices= LIST_Tipe, widget=forms.Select(attrs={'class': 'form-control'})) 
    nama = forms.CharField(label="Nama", widget=forms.TextInput(attrs={'class': 'form-control'}))
    statusmilik = forms.ChoiceField(label="Status Kepemilikan", choices= LIST_Status, widget=forms.Select(attrs={'class': 'form-control'})) 
    jalan = forms.CharField(label="Jalan", widget=forms.TextInput(attrs={'class': 'form-control'}))
    kelurahan = forms.CharField(label="Kelurahan",  widget=forms.TextInput(attrs={'class': 'form-control'}))
    kecamatan = forms.CharField(label="Kecamatan", widget=forms.TextInput(attrs={'class': 'form-control'}))
    kabkot = forms.CharField(label="Kabupaten/Kota",  widget=forms.TextInput(attrs={'class': 'form-control'}))
    prov = forms.CharField(label="Provinsi", widget=forms.TextInput(attrs={'class': 'form-control'}))


class UpdateFaskes(forms.Form):
    LIST_Tipe = []
    LIST_Status = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT DISTINCT tipe, tipe FROM SIRUCO.FASKES")
            self.fields['tipe'].choices = cursor.fetchall()

            cursor.execute("SELECT DISTINCT statusmilik, statusmilik FROM SIRUCO.FASKES")
            self.fields['statusmilik'].choices = cursor.fetchall()

    kode = forms.CharField(label='Kode Faskes', widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    tipe = forms.ChoiceField(label="Tipe", choices= LIST_Tipe, widget=forms.Select(attrs={'class': 'form-control'})) 
    nama = forms.CharField(label="Nama",  widget=forms.TextInput(attrs={'class': 'form-control'}))
    statusmilik = forms.ChoiceField(label="Status Kepemilikan", choices= LIST_Status, widget=forms.Select(attrs={'class': 'form-control'})) 
    jalan = forms.CharField(label="Jalan", widget=forms.TextInput(attrs={'class': 'form-control'}))
    kelurahan = forms.CharField(label="Kelurahan", widget=forms.TextInput(attrs={'class': 'form-control'}))
    kecamatan = forms.CharField(label="Kecamatan", widget=forms.TextInput(attrs={'class': 'form-control'}))
    kabkot = forms.CharField(label="Kabupaten/Kota",  widget=forms.TextInput(attrs={'class': 'form-control'}))
    prov = forms.CharField(label="Provinsi",  widget=forms.TextInput(attrs={'class': 'form-control'}))



class DateInput (forms.DateInput):
    input_type = 'date'

class reservasiJadwalFaskes(forms.Form):
    LIST_Faskes = []
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT DISTINCT kode, kode FROM SIRUCO.FASKES")
            self.fields['kode'].choices = cursor.fetchall()

    kode = forms.ChoiceField(label="Faskes", choices= LIST_Faskes, widget=forms.Select(attrs={'class': 'form-control'})) 
    shift = forms.CharField(label="Shift",  widget=forms.TextInput(attrs={'class': 'form-control'}))
    tanggal = forms.DateField(label="Tanggal", widget=DateInput)
