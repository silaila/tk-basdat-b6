from django.urls import path, include
from . import views

app_name = 'faskes'
urlpatterns = [ 
	path('ReservasiFaskes/', views.ReservasiFaskes, name='ReservasiFaskes'),
    path('listReservasiFaskes/', views.listReservasiFaskes, name='listReservasiFaskes'),
    path('deleteFaskes/', views.deleteFaskes, name='deleteFaskes'),
    path('updateFaskes/', views.updateFaskes, name='updateFaskes'),
    path('detailFaskes/', views.detailFaskes, name='detailFaskes'),
    path('buatJadwalFaskes/', views.buatJadwalFaskes, name='buatJadwalFaskes'),
    path('listJadwalFaskes/', views.listJadwalFaskes, name='listJadwalFaskes'),

]