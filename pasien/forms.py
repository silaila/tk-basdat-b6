from django import forms
from django.forms import Form
from django.db import connection, transaction, IntegrityError
from django.core.paginator import Paginator

class PendaftaranPasien(forms.Form):
    
    nik = forms.CharField(label='NIK', widget=forms.TextInput(attrs={'class': 'form-control'}))
    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={'class':'form-control'}))
    notelp = forms.CharField(label='No telepon', widget=forms.TextInput(attrs={'class':'form-control'}))
    nohp = forms.CharField(label='No HP', widget=forms.TextInput(attrs={'class':'form-control'}))

    KTP_Jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Prov = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={'class':'form-control'}))
    
    dom_jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_prov = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={'class':'form-control'}))
    
class UpdatePasien(forms.Form):
    pendaftar = forms.CharField(label='Pendaftar', widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))
    nik = forms.CharField(label='NIK', widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))
    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={'class':'form-control', 'readonly': True}))
    notelp = forms.CharField(label='No telepon', widget=forms.TextInput(attrs={'class':'form-control'}))
    nohp = forms.CharField(label='No HP', widget=forms.TextInput(attrs={'class':'form-control'}))

    KTP_Jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={'class':'form-control'}))
    KTP_Prov = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={'class':'form-control'}))
    
    dom_jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={'class':'form-control'}))
    dom_prov = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={'class':'form-control'}))
    