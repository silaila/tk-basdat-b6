from django.urls import path, include
from . import views

app_name = 'pasien'
urlpatterns = [ 
    path('create/', views.pasien_form_view, name='create-pasien'),
    path('list/', views.list_pasien, name='list-pasien'),
    path('detailPasien/', views.detailPasien, name='detailPasien'),
    path('deletePasien/', views.deletePasien, name='deletePasien'),
    path('updatePasien/', views.updatePasien, name='updatePasien'),
    # path('detail-pasien/<id>', views.detail_pasien, name='detail_pasien'),
    # path('delete-pasien/<id>', views.delete_pasien, name='delete_pasien<id>'),
]