from django.contrib.sessions.models import Session
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from .forms import PendaftaranPasien, UpdatePasien
from django.db import connection
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse


def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
    ]
#---------------- PENDAFTARAN ----------------#
def pasien_form_view(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")

        cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",
                        [request.session.get("username")])
        user_data = cursor.fetchone()[0]
        print (user_data)

    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = PendaftaranPasien(request.POST)
            if form.is_valid():

                nik = request.POST.get('nik')
                # id_pendaftar = request.POST.get('username')
                nama = request.POST.get('nama')
                # status = request.POST.get('status')
                nohp = request.POST.get('nohp')
                notelp = request.POST.get('notelp')
                KTP_Jalan = request.POST.get('KTP_Jalan')
                KTP_Kelurahan = request.POST.get('KTP_Kelurahan')
                KTP_Kecamatan = request.POST.get('KTP_Kecamatan')
                KTP_Kabkot = request.POST.get('KTP_Kabkot')
                KTP_Prov = request.POST.get('KTP_Prov')
                dom_jalan = request.POST.get('dom_jalan')
                dom_kelurahan = request.POST.get('dom_kelurahan')
                dom_kecamatan = request.POST.get('dom_kecamatan')
                dom_kabkot = request.POST.get('dom_kabkot')
                dom_prov = request.POST.get('dom_prov')
                peran = 'PASIEN'

                #Insert into PASIEN
                with connection.cursor() as cursor:

                    cursor.execute("SET search_path TO SIRUCO;")
                    cursor.execute("INSERT INTO PASIEN VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}');".format(
                                nik, user_data, nama, KTP_Jalan, KTP_Kelurahan, KTP_Kecamatan,
                                KTP_Kabkot,	KTP_Prov, dom_jalan, dom_kelurahan,	dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp))
                return redirect('pasien:list-pasien')
        form = PendaftaranPasien()
        args = {
            'user' : user_data,
            'form' : form,
        }
        print("di deket return")
        print (user_data)
        return render(request, 'create-pasien.html', args)

#---------------- LIST PASIEN ----------------#
def list_pasien(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username)

        with connection.cursor() as cursor:
            cursor.execute("SELECT username FROM SIRUCO.PENGGUNA_PUBLIK WHERE username= '{}';".format(username))
            pendaftar = dictfecthall(cursor)[0]
            idPendaftar = pendaftar['username']
    
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.PASIEN WHERE idpendaftar = '{}';"
                            .format(idPendaftar))
            data_pasien = dictfecthall(cursor)

        return render(request, 'list-pasien.html', {'pasien': data_pasien})

# # ----------------- DETAIL PASIEN---------------------- #
def detailPasien(request):
    with connection.cursor() as cursor:
        print('sebelum set siruco')
        cursor.execute("SET search_path to SIRUCO;")
        print ('before tes dok')
        username = request.session["username"]

    if request.method == 'GET':
        print('tes update pasien get')
        nik = request.GET.get('nik')
        nama = request.GET.get('nama')

        isi = {
            'nik': nik,
            'nama' : nama}

        print(isi)
    
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.PASIEN WHERE nik='{}';".format(nik))
        data_pasien = dictfecthall(cursor)
        print (data_pasien)

    return render(request, 'detail-pasien.html', {'data' : data_pasien})

#--------------------UPDATE PASIEN---------------------
def updatePasien(request):
    print('masuk update')
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

        if request.method == 'POST':
            nik = request.POST.get('nik')
            nama = request.POST.get('nama')
            nohp = request.POST.get('nohp')
            notelp = request.POST.get('notelp')
            KTP_Jalan = request.POST.get('KTP_Jalan')
            KTP_Kelurahan = request.POST.get('KTP_Kelurahan')
            KTP_Kecamatan = request.POST.get('KTP_Kecamatan')
            KTP_Kabkot = request.POST.get('KTP_Kabkot')
            KTP_Prov = request.POST.get('KTP_Prov')
            dom_jalan = request.POST.get('dom_jalan')
            dom_kelurahan = request.POST.get('dom_kelurahan')
            dom_kecamatan = request.POST.get('dom_kecamatan')
            dom_kabkot = request.POST.get('dom_kabkot')
            dom_prov = request.POST.get('dom_prov')
            

            #UPDATE TABLE PASIEN 
            with connection.cursor() as cursor:
                cursor.execute(
                """
                UPDATE SIRUCO.PASIEN SET nohp = '{}' , notelp ='{}', ktp_jalan='{}',
                ktp_kelurahan='{}', ktp_kecamatan='{}' , ktp_kabkot = '{}', ktp_prov='{}',
                dom_jalan='{}', dom_kelurahan='{}', dom_kecamatan='{}', dom_kabkot='{}', dom_prov='{}' 
                WHERE nik='{}' AND nama='{}';
                """
                .format(
                nohp, notelp, KTP_Jalan,
                KTP_Kelurahan, KTP_Kecamatan, KTP_Kabkot, KTP_Prov,  
                dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot,
                dom_prov, nik, nama))
                return redirect('pasien:list-pasien')
        
        elif request.method == 'GET':
            nik = request.GET.get('nik')
            nama = request.GET.get('nama')

            with connection.cursor() as cursor:
                cursor.execute(
                """
                SELECT * FROM SIRUCO.PASIEN   
                WHERE nik='{}' AND nama='{}';
                """
                .format(
                nik, nama))
                data_pasien = dictfecthall(cursor)[0]
                isi = {
                    'pendaftar' : username,
                    'nik' : nik,
                    'nama' : nama,
                    'nohp' : data_pasien['nohp'],
                    'notelp' : data_pasien['notelp'],
                    'KTP_Jalan' : data_pasien['ktp_jalan'],
                    'KTP_Kelurahan' : data_pasien['ktp_jalan'],
                    'KTP_Kecamatan' : data_pasien['ktp_kecamatan'],
                    'KTP_Kabkot' : data_pasien['ktp_kabkot'],
                    'KTP_Prov' : data_pasien['ktp_prov'],
                    'dom_jalan' : data_pasien['dom_jalan'],
                    'dom_kelurahan' : data_pasien['dom_kelurahan'],
                    'dom_kecamatan' : data_pasien['dom_kecamatan'],
                    'dom_kabkot' : data_pasien['dom_kabkot'],
                    'dom_prov' : data_pasien['dom_prov']
                } 

            
            if nik is None or nama is None:
                return redirect('pasien:list-pasien')

            if isi is None:
                return redirect('pasien:list-pasien')

            form=UpdatePasien(initial=isi)
        return render(request, 'update-pasien.html', {'form': form})

# ------------- DELETE PASIEN --------------------- #
def deletePasien(request):
    with connection.cursor() as cursor:
        if request.method == 'GET':
            nik = request.GET.get('nik')
            nama = request.GET.get('nama')
            
            with connection.cursor() as cursor:
                cursor.execute("SET search_path to SIRUCO;")
                cursor.execute(
                    """
                    DELETE FROM MEMERIKSA
                    WHERE nik_pasien = '{}';
                    DELETE FROM PASIEN
                    WHERE nik='{}';
                    """
                    .format(nik,nik))

        return redirect('pasien:list-pasien')
