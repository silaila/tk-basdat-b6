from django.urls import path, include
from . import views

app_name = 'transaksibooking'
urlpatterns = [ 
    path('listTransaksiBooking/', views.listTransaksiBooking, name='listTransaksiBooking'),
]