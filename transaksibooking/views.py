from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
# from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection

def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
        ]
        
def listTransaksiBooking(request) : 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username)


        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_BOOKING;")
            data_transbook = dictfecthall(cursor)
        return render(request, 'list-transaksibooking.html', {'transbook': data_transbook})