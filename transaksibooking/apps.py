from django.apps import AppConfig


class TransaksibookingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksibooking'
