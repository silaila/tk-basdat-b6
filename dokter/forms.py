from django import forms
from django.forms import Form
from django.db import connection, transaction, IntegrityError

class CreateAppointment(forms.Form):
    LIST_NIK= []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK, NIK FROM SIRUCO.PASIEN")
            self.fields['nik'].choices = cursor.fetchall()

    nik = forms.ChoiceField(label='NIK Pasien',  choices= LIST_NIK, widget=forms.Select(attrs={'class': 'form-control'}))
    email = forms.CharField(label="Email Dokter",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    kode_faskes = forms.CharField(label="Kode Faskes", widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    tanggal = forms.CharField(label="Tanggal Praktek",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
    shift = forms.CharField(label="Shift Praktek",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))

class UpdateAppointment(forms.Form):
	nik = forms.CharField(label="NIK Pasien", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	email = forms.CharField(label="Email Dokter", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	kode_faskes = forms.CharField(label="Kode Faskes", required=False, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	tanggal = forms.CharField(label="Tanggal Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
	shift = forms.CharField(label="Shift Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
	rekomendasi = forms.CharField(label="Rekomendasi", required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))

class CreateRuanganRS(forms.Form):
    harga_ruangan = forms.CharField(label="Harga Ruangan", required=True, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipe = forms.CharField(label="Tipe", required=True, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control'}))  

class UpdateRuanganRS(forms.Form):
    koders = forms.CharField(label='Kode Rumah Sakit', required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))
    koderuangan = forms.CharField(label="Kode Ruangan", required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': True}))
    harga = forms.CharField(label="Harga Ruangan", required=True, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipe = forms.CharField(label="Tipe", required=True, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control'}))

class CreateBedRS(forms.Form):
	KODE_RS_CHOICES = []

	kode_rs = forms.ChoiceField(label='Kode Rumah Sakit', required=True, choices= KODE_RS_CHOICES, widget=forms.Select(attrs={
		'class': 'form-control',
		'id' : 'kode_rs',
		'required' : True}))

	def __init__(self, *args, **kwargs):
		super(CreateBedRS, self).__init__(*args, **kwargs)
		with connection.cursor() as cursor:
			cursor.execute("SELECT Kode_Faskes, Kode_Faskes FROM SIRUCO.RUMAH_SAKIT")
			self.fields['kode_rs'].choices = cursor.fetchall()