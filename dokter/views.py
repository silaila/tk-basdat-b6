from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection
import datetime

def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
        ]

#APPOINTMENT ------------------------------------------------------------------------------------------------------
def createAppointment(request):
    # print (getrole)
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username) 
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.JADWAL_DOKTER")
        data_appointment = dictfecthall(cursor)
    return render(request, 'create-appointment.html', {'appointment': data_appointment})

def formAppointment(request): 
    with connection.cursor() as cursor:
        print('sebelum set siruco')
        cursor.execute("SET search_path to SIRUCO;")
        print ('before tes dok')
        username = request.session["username"]
        
        if request.method == 'POST':
            # form = createAppointment(request.POST)
            # if form.is_valid():
            nik = request.POST.get('nik_pasien')
            email = request.POST.get('email')
            kode = request.POST.get('kode_faskes')
            shift = request.POST.get('shift')
            tanggal = request.POST.get('tanggal')
            rekomendasi = '...'
            print(nik)
            isi = {'email': email, 
                'kode_faskes': kode, 
                'tanggal': tanggal,
                'shift': shift}

            with connection.cursor() as cursor:
                cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [email])
                no_str = cursor.fetchone()[0]
                print(no_str)
                
            if no_str is None:
                return redirect('dokter:formAppointment')
            print('tes dokter')
            try: 
                print('tes dokter 2')
            #Insert into MEMERIKSA 
                with connection.cursor() as cursor:
                    
                    print('tes dok')
                    cursor.execute("SET search_path TO SIRUCO;")
                    print(nik, no_str, email, kode, shift, tanggal, rekomendasi)
                    cursor.execute("INSERT INTO MEMERIKSA "
                                    "VALUES ('{}', '{}', '{}', '{}', '{}','{}', '{}');"
                                    .format(nik, no_str, email, kode, shift, tanggal, rekomendasi))
                    print('tes dokk')
                
                return redirect('dokter:listAppointment')

            except: 
                print('tes dokter 3')
                form=CreateAppointment(initial=isi)
                return render(request, 'form-appointment.html', 
                {'form': form, 'message': 'Tidak bisa membuat perjanjian dengan dokter, silahkan pilih jadwal lainnya.'})
        
        elif request.method == 'GET':
            print('tes dokter 4')
            username = request.GET.get('username')
            kode_faskes = request.GET.get('kode_faskes')
            shift = request.GET.get('shift')
            tanggal = request.GET.get('tanggal')
            print(username, kode_faskes, shift, tanggal)

            isi = {
                'email': username, 
                'kode_faskes': kode_faskes, 
                'tanggal': tanggal,
                'shift': shift} 

            print(isi)
            if username is None or kode_faskes is None or shift is None or tanggal is None:
                print("masuk none yg atas")
                return redirect('dokter:createAppointment')

            if isi is None:
                return redirect('dokter:createAppointment')

            form=CreateAppointment(initial=isi)
        return render(request, 'form-appointment.html', {'form': form})

def ajaxAppointment(request):
    print("MASUK AJAX")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        pendaftar = request.session["username"]  
    print(pendaftar)
    if request.session.get("role") == "PENGGUNA_PUBLIK":
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK, NIK FROM SIRUCO.PASIEN where IdPendaftar = %s", [pendaftar])
            data = cursor.fetchall()
    else:
        print('masuk else ajax')
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK, NIK FROM SIRUCO.PASIEN")
            data = cursor.fetchall()
    nik_pasien = []
    for i in data:
        nik_pasien.append(i[0])
    print(nik_pasien)
    return JsonResponse({'id': nik_pasien})

def listAppointment(request) : 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        print(username)

        if request.session.get('role') == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.MEMERIKSA;")
                data_appointment = dictfecthall(cursor)

        if request.session.get('role') == 'PENGGUNA_PUBLIK':
            with connection.cursor() as cursor:
                cursor.execute(
                """
                SELECT * FROM SIRUCO.MEMERIKSA M, SIRUCO.PASIEN P
                WHERE M.nik_pasien = P.nik AND P.idPendaftar = '{}';
                """
                .format(username))
                data_appointment = dictfecthall(cursor)

        if request.session.get('role') == 'DOKTER':
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.MEMERIKSA WHERE username_dokter = '{}';"
                .format(username))
                data_appointment = dictfecthall(cursor)

        return render(request, 'list-appointment.html', {'appointment': data_appointment})

def deleteAppointment(request):
    with connection.cursor() as cursor:
        if request.method == 'GET':
            nik = request.GET.get('nik')
            username = request.GET.get('username')
            kode_faskes = request.GET.get('kode_faskes')
            shift = request.GET.get('shift')
            tanggal = request.GET.get('tanggal')
            print('apa')
            print(nik, username, kode_faskes, shift, tanggal)
            print('apa1')
            with connection.cursor() as cursor:
                cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username='{}' LIMIT 1;".format(username))
                no_str = cursor.fetchone()[0]
                print(no_str)
            print('apa2')
            with connection.cursor() as cursor:
                cursor.execute("SET search_path to SIRUCO;")
                cursor.execute(
                    '''
                    DELETE FROM SIRUCO.MEMERIKSA WHERE nik_pasien='{}' 
                    AND username_dokter='{}' AND nostr='{}' AND kode_faskes= '{}' 
                    AND praktek_shift= '{}' AND praktek_tgl='{}';
                    '''.format(nik, username, no_str, kode_faskes, shift, tanggal))
            print('apa3')
        return redirect('dokter:listAppointment')

def updateAppointment(request):

    with connection.cursor() as cursor:
        print('sebelum set siruco')
        cursor.execute("SET search_path to SIRUCO;")
        print ('before tes dok')
        username = request.session["username"]

        if request.method == 'POST':
            print("masuk post update")
            nik = request.POST.get('nik')
            email = request.POST.get('email')
            kode = request.POST.get('kode_faskes')
            shift = request.POST.get('shift')
            tanggal = request.POST.get('tanggal')
            rekomendasi = request.POST.get('rekomendasi')
            print(rekomendasi)

            isi = {
                'nik': nik,
                'email': username, 
                'kode_faskes': kode, 
                'tanggal': tanggal,
                'shift': shift,
                'rekomendasi': rekomendasi
                }
         
            try: 
                print('masuk try')
                if rekomendasi is None or rekomendasi == "":
                    raise ValidationError("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu")

                with connection.cursor() as cursor:
                    cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                    cursor.execute("SELECT NoSTR FROM DOKTER where username=%s LIMIT 1;", [email])
                    no_str = cursor.fetchone()[0]
                    
                if no_str is None:
                    print("masuk none yg no str")
                    return redirect('dokter:formAppointment')
                print('tes dokter')
                
                print('tes dokter 2')
                #UPDATE TABLE MEMERIKSA 
                with connection.cursor() as cursor:
                    print("masuk update table")
                    cursor.execute("SET search_path to SIRUCO;")
                    cursor.execute(
                    """
                    UPDATE MEMERIKSA SET rekomendasi= '{}'
                    WHERE nik_pasien='{}' AND username_dokter='{}' 
                    AND kode_faskes= '{}' 
                    AND praktek_shift= '{}' AND praktek_tgl='{}';
                    """
                    .format(
                    rekomendasi, 
                    nik, email, 
                    kode,
                    shift, tanggal))
                    return redirect('dokter:listAppointment')
                
            except :
                print('masuk sini')
                form=UpdateAppointment(initial=isi)
                return render(request, 'update-appointment.html', 
                {'message': 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu'})
            return redirect('dokter:listAppointment') 
        
        elif request.method == 'GET':
            print('tes dokter4')
            nik = request.GET.get('nik')
            username = request.GET.get('username')
            kode_faskes = request.GET.get('kode_faskes')
            shift = request.GET.get('shift')
            tanggal = request.GET.get('tanggal')

            isi = {
                'nik': nik,
                'email': username, 
                'kode_faskes': kode_faskes, 
                'tanggal': tanggal,
                'shift': shift} 

            print(isi)

            if nik is None or username is None or kode_faskes is None or shift is None or tanggal is None:
                print("update none banyak")
                return redirect('dokter:listAppointment')

            if isi is None:
                print("update isi none")
                return redirect('dokter:listAppointment')

            form=UpdateAppointment(initial=isi)
        return render(request, 'update-appointment.html', {'form': form})

#JADWAL DOKTER-----------------------------------------------------------------------------------------------------
def listJadwal(request):
    # print('masuk')
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

        peran = request.session.get("role")
        print(peran)

        if request.session.get("role") == 'DOKTER':
            print('masuk if')
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.JADWAL_DOKTER WHERE username= '{}';".format(username))
                data_jadwal = dictfecthall(cursor)

        else :
            print('masuk else')
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.JADWAL_DOKTER;")
                data_jadwal = dictfecthall(cursor)
        return render(request, 'list-jadwal-dokter.html', {'jadwal': data_jadwal})

def readJadwal(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"] 

        with connection.cursor() as cursor:
            cursor.execute(
                """select * from siruco.jadwal EXCEPT 
                (select kode_faskes,shift,tanggal from siruco.jadwal_dokter j 
                WHERE username= '{}');
                """.format(username))
            data_jadwal = dictfecthall(cursor)
        return render(request, 'create-jadwal-dokter.html', {'jadwal': data_jadwal})

def createJadwal(request):
    print('masuk create')
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

        peran = request.session.get("role")
        print(peran)

        if request.method == 'GET':
            print('masuk get')
            print(username) 
            kode_faskes = request.GET.get('kode_faskes')
            shift = request.GET.get('shift')
            tanggal = request.GET.get('tanggal') 
            print(username, kode_faskes, shift, tanggal)

            try: 
                with connection.cursor() as cursor:
                    print('masuk ga?')
                    cursor.execute("SELECT NoSTR FROM SIRUCO.DOKTER where username=%s LIMIT 1;", [username])
                    no_str = cursor.fetchone()[0]

                    cursor.execute("""
                    SELECT * FROM SIRUCO.JADWAL_DOKTER 
                    where nostr='{}' AND username='{}' AND kode_faskes='{}' 
                    AND shift='{}' AND tanggal='{}' LIMIT 1;
                    """.format(no_str, username, kode_faskes, shift, tanggal))
                    data_jadwal = cursor.fetchone()

                    print(data_jadwal) 
                    cursor.execute("SET SEARCH_PATH TO SIRUCO;")
                    cursor.execute("INSERT INTO JADWAL_DOKTER VALUES('{}', '{}', '{}', '{}', '{}', '{}')".
                                    format(no_str, username, kode_faskes, shift, tanggal, 0))
                    print('selesai')
            
            except: 
                return render(request, 'create-jadwal-dokter.html', 
                {'message': 'Jadwal dokter yang dipilih sudah tersedia.'})
            return redirect('dokter:listJadwal') 

#RUANGAN DAN BED RS-----------------------------------------------------------------------------------------------
def createRuanganRS(request):
    print('masuk create')
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

    form = CreateRuanganRS()
    if request.method == 'POST':
        print("masuk post create")
        kode_rs = request.POST.get('kodeRS')
        kode_ruangan = request.POST.get('ruanganRS')
        harga_ruangan = request.POST.get('harga_ruangan')
        tipe = request.POST.get('tipe')
        jmlbed = 0

        print(kode_rs, kode_ruangan, tipe, jmlbed, harga_ruangan)
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO SIRUCO.RUANGAN_RS VALUES('{}', '{}', '{}', '{}', '{}');"
                            .format(kode_rs, kode_ruangan, tipe, jmlbed, harga_ruangan))
        return redirect('dokter:listRuanganRS')
    return render(request, 'create-ruangan-rs.html', {'form': form})

def listRuanganRS(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

        with connection.cursor() as cursor:
            cursor.execute("SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.RUANGAN_RS")
            data_ruanganrs = dictfecthall(cursor)
            print('data list')
            print(data_ruanganrs)
    return render(request, "list-ruangan-rs.html", {'data': data_ruanganrs})

def ajaxKodeRS(request): 
    print("masuk ajax kdoe ")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_faskes FROM SIRUCO.RUMAH_SAKIT")
        data = cursor.fetchall()
        print(data)
    kode_rs = []
    for i in data:
        kode_rs.append(i[0])
    return JsonResponse({'id': kode_rs})

def ajaxRuanganRS(request): 
    print("masuk ajax ruangan")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
    kode_rs = request.GET.get('kode_rs')
    newkode = ''
    with connection.cursor() as cursor:
            cursor.execute(
                """
                SELECT koderuangan FROM SIRUCO.RUANGAN_RS 
                WHERE koders = '{}' ORDER BY koderuangan DESC LIMIT 1;
                """.format(kode_rs))
            try:
                print('masuk try')
                old = cursor.fetchone()[0]
                print('old')
                print(old)
                newkode = old[0:2] + str(int(old[2:]) + 1)
            except: 
                print('masuk except')
                newkode = "R" + kode_rs
            print(newkode)
    return JsonResponse({'id': newkode})

def updateRuanganRS(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

    if request.method == "POST":
        tipe = request.POST['tipe']
        harga = request.POST['harga']
        koders = request.POST['koders']
        koderuangan = request.POST['koderuangan']
        print(tipe, harga, koders, koderuangan)
        
        isi = {
            'tipe' : tipe,
            'harga' : harga,
            'koders' : koders,
            'koderuangan' : koderuangan,
        }

        print(isi) 

        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
            """
            UPDATE RUANGAN_RS SET tipe = '{}', harga = '{}'
            WHERE koders='{}' AND koderuangan ='{}' 
            """
            .format(tipe, harga, koders, koderuangan))
        return redirect("dokter:listRuanganRS")

    elif request.method == "GET":
        print('masuk get update')
        koders = request.GET.get('koders')
        koderuangan = request.GET.get('koderuangan') 
        print(koders, koderuangan)

        with connection.cursor() as cursor:
            print('masuk cursor')
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
            """
            SELECT * FROM SIRUCO.RUANGAN_RS WHERE 
            koders='{}' AND koderuangan='{}' LIMIT 1; 
            """
            .format(koders, koderuangan))
            print('berhasil sql')
            
            data_ruangan = cursor.fetchone()
            print(data_ruangan) 

            columns = [col[0] for col in cursor.description]
            data_ruanganrs = dict(zip(columns, data_ruangan))
            
            print(data_ruanganrs)

        form = UpdateRuanganRS(initial=data_ruanganrs)
    return render(request, "update-ruangan-rs.html", {'form' : form })

def createBedRS(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        form = UpdateRuanganRS()
    
    form = CreateBedRS()
    if request.method == 'POST':
        kode_rs = request.POST.get('kode_rs')
        kode_ruangan = request.POST.get('ruanganRS')
        kode_bed = request.POST.get('bedRS')
        print(kode_rs, kode_ruangan, kode_bed)
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO")
            cursor.execute(
                "INSERT INTO SIRUCO.BED_RS VALUES ('{}', '{}', '{}');"
                .format(kode_rs, kode_ruangan, kode_bed))
        return redirect('dokter:listBedRS')
    return render(request, 'create-bed-rs.html', {'form': form})

def listBedRS(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
    
    with connection.cursor() as cursor:
        cursor.execute(
            """
            select koders, koderuangan, kodebed from siruco.reservasi_rs
            where now() < tglkeluar;
            """)

        can_delete = dictfecthall(cursor)
        outkoders=[]
        outkoderuangan=[]
        outkodebed=[]
        print(can_delete)

        for i in can_delete:
            print(i['koders'])
            outkoders.append(i['koders'])
            outkoderuangan.append(i['koderuangan'])
            outkodebed.append(i['kodebed'])

        print("keluar tanggal")
        print(outkodebed)

    with connection.cursor() as cursor:
        cursor.execute(
            """
            select koders, koderuangan, kodebed from siruco.reservasi_rs;
            """)
        data_bed_and_reservasi = dictfecthall(cursor)
        allkoders=[]
        allkoderuangan=[]
        allkodebed=[]

        for i in data_bed_and_reservasi:
            allkoders.append(i['koders'])
            allkoderuangan.append(i['koderuangan'])
            allkodebed.append(i['kodebed'])

        print(allkodebed) 
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT ROW_NUMBER() over (order by 1) as nomor, * FROM SIRUCO.BED_RS;
            """)
        data_bed_rs = dictfecthall(cursor)
        print(data_bed_rs)
	
    return render(request, "list-bed-rs.html", {'allkodebed': allkodebed, 'outkodebed': outkodebed, 'data': data_bed_rs})

def ajaxRuanganBed(request):
    print("masuk kode ruangan bed")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
    
    print(request.GET.get('kode_faskes'))
    kode_faskes = request.GET.get('kode_faskes')
    print(kode_faskes)
    with connection.cursor() as cursor:
        cursor.execute(
            """
            SELECT koderuangan FROM SIRUCO.RUANGAN_RS WHERE KODERS = '{}' AND JMLBED > 0;
            """.format(kode_faskes))
        data = cursor.fetchall()
        print(data)
    list_ruangan = []
    for i in data:
        list_ruangan.append(i[0])
    print(list_ruangan)
    return JsonResponse({'id': list_ruangan})

def ajaxKodeBed(request):
    print("masuk ajax kode bed")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

    kode_ruangan = request.GET.get('kode_ruangan')
    print(request.GET.get('kode_ruangan'))
    kode_bed_new = ''
    with connection.cursor() as cursor:
            cursor.execute(
                """
                SELECT kodebed FROM SIRUCO.BED_RS WHERE koderuangan = '{}'
                ORDER BY kodebed DESC LIMIT 1;
                """.format(kode_ruangan))
            try:
                print("masuk try")
                old = cursor.fetchone()[0]
                print("old")
                print(old)
                kode_bed_new = old[0:2] + str(int(old[2:]) + 1)
                print("kode baru")
                print(kode_bed_new)
            except:
                cursor.execute(
                """
                SELECT koders FROM SIRUCO.RUANGAN_RS WHERE koderuangan = '{}' LIMIT 1;
                """.format(kode_ruangan))
                kode_faskes = cursor.fetchone()[0]
                kode_bed_new = "B" + kode_faskes
    return JsonResponse({'id': kode_bed_new})

def deleteBedRS(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  

    if request.method == 'GET':
        # kode_bed = []
        if request.GET.get('koders') is not None and request.GET.get('koderuangan') is not None and request.GET.get('kodebed') is not None:
            koders = request.GET.get('koders')
            koderuangan = request.GET.get('koderuangan')
            kodebed = request.GET.get('kodebed')
            print(koders, koderuangan, kodebed)

            with connection.cursor() as cursor:
                cursor.execute(
                    '''
                    DELETE FROM SIRUCO.BED_RS WHERE koders = '{}' 
                    AND koderuangan = '{}' AND kodebed = '{}';
                    '''
                    .format(koders, koderuangan, kodebed))
            
            return redirect('dokter:listBedRS')
