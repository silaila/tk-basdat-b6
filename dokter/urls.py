from django.urls import path, include
from . import views

app_name = 'dokter'
urlpatterns = [ 
	path('createAppointment/', views.createAppointment, name='createAppointment'),
    path('formAppointment/', views.formAppointment, name='formAppointment'),
    path('listAppointment/', views.listAppointment, name='listAppointment'),
    path('deleteAppointment/', views.deleteAppointment, name='deleteAppointment'),
    path('updateAppointment/', views.updateAppointment, name='updateAppointment'),
    path('ajaxAppointment/', views.ajaxAppointment, name='ajaxAppointment'),

    path('createJadwal/', views.createJadwal, name='createJadwal'),
    path('readJadwal/', views.readJadwal, name='readJadwal'),
    path('listJadwal/', views.listJadwal, name='listJadwal'),

	path('createRuanganRS/', views.createRuanganRS, name='createRuanganRS'),
    path('listRuanganRS/', views.listRuanganRS, name='listRuanganRS'),
    path('updateRuanganRS/', views.updateRuanganRS, name='updateRuanganRS'),
    path('ajaxKodeRS/', views.ajaxKodeRS, name='ajaxKodeRS'),
	path('ajaxRuanganRS/', views.ajaxRuanganRS, name='ajaxRuanganRS'),

    path('createBedRS/', views.createBedRS, name='createBedRS'),
    path('listBedRS/', views.listBedRS, name='listBedRS'),
    path('deleteBedRS/', views.deleteBedRS, name='deleteBedRS'),
    path('ajaxRuanganBed/', views.ajaxRuanganBed, name='ajaxRuanganBed'),
	path('ajaxKodeBed/', views.ajaxKodeBed, name='ajaxKodeBed'),

]