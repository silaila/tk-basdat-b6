from django import forms
from django.forms import Form
from django.db import connection, transaction, IntegrityError
import datetime;

class BuatReservasiHotel(forms.Form):
    print("masuk form")
    LIST_NIK= []
    LIST_KODE_HOTEL= []
    LIST_KODE_ROOM= []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # kode_hotel = ""
        with connection.cursor() as cursor:
	        cursor.execute("SELECT NIK,NIK FROM SIRUCO.PASIEN")
	        self.fields['nik'].choices = cursor.fetchall()
        with connection.cursor() as cursor:
	        cursor.execute("SELECT kode,kode FROM SIRUCO.HOTEL")
	        self.fields['kode'].choices = cursor.fetchall()
        # with connection.cursor() as cursor:
	    #     cursor.execute("SELECT koderoom,koderoom FROM SIRUCO.HOTEL_ROOM")
	    #     self.fields['koderoom'].choices = cursor.fetchall()

    # cek tglkeluar > tgl masuk
    # Tanggal Masuk
    tglmasuk = forms.DateField(label='Tanggal Masuk', initial=datetime.datetime.now())
    # print (tglmasuk)
    # Tanggal Keluar
    tglkeluar = forms.DateField(label='Tanggal Keluar', initial=datetime.datetime.now())
    # print(tglkeluar)

    def clean_tglkeluar(self):
        cleaned_data=super().clean()
        date_out = cleaned_data['tglkeluar']
        date_in = cleaned_data['tglmasuk']
        if date_out < date_in or date_out == date_in:
            raise forms.ValidationError("Masukkan tanggal keluar setelah tanggal masuk")
        return date_out

    nik = forms.ChoiceField(label='NIK Pasien',  choices= LIST_NIK, widget=forms.Select(attrs={'class': 'form-control'}))
    # print(nik)
    kode = forms.ChoiceField(label='Kode Hotel', required=True, choices= LIST_KODE_HOTEL, widget=forms.Select(attrs={'class': 'form-control', 'name':'kode', 'id': 'id_kode'}))
    # print(kode)
    koderoom =  forms.ChoiceField(label='Kode Ruangan', required=True, choices= LIST_KODE_ROOM, widget=forms.Select(attrs={'class': 'form-control', 'name':'koderoom', 'id': 'id_koderoom'}))
    # print(koderoom)

# class UpdateAppointment(forms.Form):
# 	nik = forms.CharField(label="NIK Pasien", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
# 	email = forms.CharField(label="Email Dokter", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
# 	kode_faskes = forms.CharField(label="Kode Faskes", required=False, max_length=10, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
# 	tanggal = forms.CharField(label="Tanggal Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
# 	shift = forms.CharField(label="Shift Praktek", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
# 	rekomendasi = forms.CharField(label="Rekomendasi", required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))

