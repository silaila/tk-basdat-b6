from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
from .forms import *
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection
from collections import namedtuple
import json
import datetime

def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
        ]

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def load_ruangan(request):
    kode = request.GET.get('kode')
    print('ngeprint kode')
    print(kode)
    with connection.cursor() as cursor:
        cursor.execute("SELECT koderoom FROM SIRUCO.HOTEL_ROOM WHERE kodehotel = %s", [kode])
        rooms = dictfecthall(cursor)
        
        print(rooms)
    
    return render(request, 'room_dropdown_list_options.html', {'rooms': rooms})


def formReservasiHotel(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",
                        [request.session.get("username")])
        user_data = cursor.fetchone()[0]
        print('print user_data di dalem formReservasi')
        print (user_data)    

        if request.method == 'POST':
            print('masukpost')
            form = BuatReservasiHotel(request.POST)
            print('mau masuk form')
            # if form.is_valid():
            # print("form is valid")
            nik = request.POST.get('nik')
            tglmasuk = request.POST.get('tglmasuk')
            tglkeluar = request.POST.get('tglkeluar')
            kode = request.POST.get('kode')
            koderoom = request.POST.get('koderoom')
            print ("INI KODE",kode)
            print ("INI KODE ROom",koderoom)
            print ("TANGGAL", tglmasuk)

            isi = {'nik': nik, 
                'tglmasuk': tglmasuk, 
                'tglkeluar': tglkeluar,
                'kode': kode,
                'koderoom': koderoom
            }
            
            # try: 
            with connection.cursor() as cursor:
                cursor.execute("SET search_path TO SIRUCO;")
                cursor.execute("INSERT INTO RESERVASI_HOTEL "
                                "VALUES ('{}', '{}', '{}', '{}', '{}');"
                                .format(nik, tglmasuk, tglkeluar, kode, koderoom))
                print('tes after insert')
            
            return redirect('reservasihotel:listReservasiHotel')

            # except: 
            #     print('tes room 3')
            #     form=BuatReservasiHotel(initial=isi)
            #     return render(request, 'form-reservasihotel.html', {'form': form})
    
        form = BuatReservasiHotel()
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO;")
            cursor.execute("SELECT kodehotel, koderoom FROM SIRUCO.HOTEL_ROOM;")
            kodekodean = dictfecthall(cursor)

            hotelcode=set()
            
            for x in kodekodean:
                hotelcode.add(x['kodehotel'])
            
            temp = dict.fromkeys(hotelcode, [])
            # print ("ISI TEMP:", temp)
            for k in temp:
                # print("INI K", k)
                tempList = []    
                for x in kodekodean:
                        # print ("X: ",x['kodehotel'])
                        # print ("X: ",x['koderoom'])
                        # print ("K: ",k)

                        if k == x['kodehotel']:
                            tempList.append(x['koderoom'])

                temp[k] = tempList
            

        pilihan = json.dumps(temp)

        
        args = {
            'pilihan' : pilihan,
            'user' : user_data,
            'form' : form,
        }
        # print(args)
        print("di deket return")
        print (user_data)
        return render(request, 'form-reservasihotel.html', {'arg': args})
        

        # elif request.method == 'GET':
        #     print('tes room 4')
        #     kodehotel = request.GET.get('kodehotel')
        #     koderoom = request.GET.get('koderoom')
        #     jenisbed = request.GET.get('jenisbed')
        #     tipe = request.GET.get('tipe')
        #     harga = request.GET.get('harga')
        #     # print(username, kode_faskes, shift, tanggal)

        #     isi = {
        #         'kodehotel': kodehotel, 
        #         'koderoom': koderoom, 
        #         'jenisbed': jenisbed,
        #         'tipe': tipe,
        #         'harga': harga} 

        #     print(isi)
        #     if username is None or kodehotel is None:
        #         print("masuk none yg atas")
        #         return redirect('ruanganhotel:formHotelRoom')

        #     if isi is None:
        #         return redirect('ruanganhotel:formHotelRoom')

        #     form=BuatHotelRoom(initial=isi)
        #     return render(request, 'form-hotelroom.html', {'form': form})

def listReservasiHotel(request) : 
    now = datetime.datetime.now()
    print(now.strftime("%d-%m-%Y"))

    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]  
        role = request.session.get('role') 

        if role=="PENGGUNA_PUBLIK":
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.RESERVASI_HOTEL, SIRUCO.PASIEN  WHERE KodePasien=NIK AND IdPendaftar='{}';".format(username))
                list_reservasihotel = dictfecthall(cursor)
            return render(request, 'list-reservasihotel.html', {'reservasi': list_reservasihotel,'hariini':now.strftime("%d-%m-%Y")})
        
        else:
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.RESERVASI_HOTEL;")
                list_reservasihotel = dictfecthall(cursor)
            return render(request, 'list-reservasihotel.html', {'reservasi': list_reservasihotel,'hariini':now.strftime("%d-%m-%Y")})

def deleteReservasiHotel(request):
    with connection.cursor() as cursor:
        if request.method == 'GET':
            nik = request.GET.get('nik')
            tglmasuk = request.GET.get('tglmasuk')

            print(nik, tglmasuk)
            # with connection.cursor() as cursor:
            #     cursor.execute("SELECT idPendaftar FROM SIRUCO.PASIEN where nik='{}' LIMIT 1;".format(nik))
            #     idPasien = cursor.fetchone()[0]
            
            with connection.cursor() as cursor:
                cursor.execute("SET search_path to SIRUCO;")
                cursor.execute(
                    """
                    DELETE FROM TRANSAKSI_BOOKING
                    WHERE kodepasien='{}' AND tglmasuk='{}';
                    DELETE FROM TRANSAKSI_HOTEL
                    WHERE kodepasien = '{}';
                    DELETE FROM RESERVASI_HOTEL
                    WHERE nik='{}' AND tglmasuk='{}';
                    """
                    .format(nik,tglmasuk,nik,nik,tglmasuk))
                
            print('apa3')
        return redirect('reservasihotel:listReservasiHotel')


# def updateHotelRoom(request):
#     with connection.cursor() as cursor:
#         print('sebelum set siruco')
#         cursor.execute("SET search_path to SIRUCO;")
#         print ('before tes dok')
#         username = request.session["username"]

#         if request.method == 'POST':
#             print("masuk post update")
#             kodehotel = request.POST.get('kode')
#             koderoom = request.POST.get('koderoom')
#             jenisbed = request.POST.get('jenisbed')
#             tipe = request.POST.get('tipe')
#             harga = request.POST.get('harga')

#             # isi = {'kodehotel': kodehotel, 
#             #         'koderoom': koderoom, 
#             #         'jenisbed': jenisbed,
#             #         'tipe': tipe,
#             #         'harga': harga}
#             # print(isi) 

#             # with connection.cursor() as cursor:
#             #     cursor.execute("SET SEARCH_PATH TO SIRUCO;")
#             #     cursor.execute("SELECT koderoom FROM HOTEL_ROOM where koderoom=%s LIMIT 1;", [koderoom])
#             #     kode_kamar = cursor.fetchone()[0]
                
#             # if kode_kamar is None:
#             #     print("masuk none yg no str")
#             #     return redirect('ruanganhotel:formHotelRoom')
#             # print('tes room')
            
#             # print('tes room 2')
#             #UPDATE TABLE MEMERIKSA 
#             with connection.cursor() as cursor:
#                 print("masuk update table")
#                 cursor.execute("SET search_path to SIRUCO;")
#                 cursor.execute(
#                 """
#                 UPDATE HOTEL_ROOM SET jenisbed='{}', tipe='{}', harga='{}'
#                 WHERE koderoom='{}' AND kodehotel='{}';
#                 """
#                 .format(
#                 jenisbed, 
#                 tipe, harga, 
#                 koderoom, kodehotel))
#                 return redirect('ruanganhotel:listHotelRoom')
        
#         elif request.method == 'GET':
#             print('tes room 4')
#             kode = request.GET.get('kode')
#             koderoom = request.GET.get('koderoom')
#             print(kode, koderoom)
#             # jenisbed = request.GET.get('jenisbed')
#             # tipe = request.GET.get('tipe')
#             # harga = request.GET.get('harga')

#             # isi = {'kodehotel': kodehotel, 
#             #         'koderoom': koderoom, 
#             #         'jenisbed': jenisbed,
#             #         'tipe': tipe,
#             #         'harga': harga} 

#             # print(isi)

#             # if kodehotel is None or koderoom is None:
#             #     print("update none banyak")
#             #     return redirect('ruanganhotel:listHotelRoom')

#             # if isi is None:
#             #     print("update isi none")
#             #     return redirect('ruanganhotel:listHotelRoom')

#             with connection.cursor() as cursor:
#                 print('ambil data')
#                 cursor.execute(
#                 """
#                 SELECT * FROM SIRUCO.HOTEL_ROOM   
#                 WHERE koderoom='{}';
#                 """
#                 .format(
#                 koderoom))
#                 data_hotelroom = dictfecthall(cursor)
#                 print ('isi data hotel')
#                 print(data_hotelroom)
#                 isi = {
#                     'kode': kode, 
#                     'koderoom': koderoom,
#                     'jenisbed': data_hotelroom[0]['jenisbed'],
#                     'tipe': data_hotelroom[0]['tipe'],
#                     'harga': data_hotelroom[0]['harga']
#                 } 
#             print(isi)
#             if koderoom is None or kode is None:
#                 return redirect('ruanganhotel:listHotelRoom')

#             if isi is None:
#                 return redirect('ruanganhotel:listHotelRoom')

#             form=UpdateHotelRoom(initial=isi)
#         return render(request, 'update-hotelroom.html', {'form': form})