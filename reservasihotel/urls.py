from django.urls import path, include
from . import views

app_name = 'reservasihotel'
urlpatterns = [ 
	
    path('formReservHotel/', views.formReservasiHotel, name='formReservasiHotel'),
    path('listReservHotel/', views.listReservasiHotel, name='listReservasiHotel'),
    # path('ajax/loadroom/', views.load_ruangan, name="loadruangan"),
    # path('updateReservHotel/', views.updateHotelRoom, name='updateHotelRoom'),
    # path('createAppointment/', views.createAppointment, name='createAppointment'),
    path('deleteReservHotel/', views.deleteReservasiHotel, name='deleteReservasiHotel'),

]