from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sessions.models import Session
from django.db import connection, transaction, IntegrityError
from .forms import ReservasiRS, UpdateReservasiRS
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from homepage.views import user_login_required 
from django.urls import reverse
from users.views import getrole
from django.db import connection
import datetime


# Create your views here.
def dictfecthall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row)) 
        for row in cursor.fetchall()
    ]

#Reservasi RS ---------------------------------------------------------------------------------------------------
def reservasiRS(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

        print("mengambil template")
        form = ReservasiRS()
        args = {
            'form' : form,
        }
        if request.method == 'POST':
            form = ReservasiRS(request.POST)
            if form.is_valid():
                kodepasien = request.POST.get('nik')
                tglmasuk = request.POST.get('tanggal_masuk')
                tglkeluar = request.POST.get('tanggal_keluar')
                koders = request.POST.get('kode_rs')
                koderuangan = request.POST.get('kode_ruangan')
                kodebed = request.POST.get('kode_bed')

                isi = {'nik': kodepasien, 
                    'tglmasuk': tglmasuk, 
                    'tglkeluar': tglkeluar,
                    'koders': koders,
                    'koderuangan': koderuangan,
                    'kodebed': kodebed,
                }
                
                try: 
                    with connection.cursor() as cursor:
                        cursor.execute("SET search_path TO SIRUCO;")
                        cursor.execute("INSERT INTO reservasi_rs "
                                        "VALUES ('{}', '{}', '{}', '{}', '{}','{}','{});"
                                        .format(kodepasien,tglmasuk,tglkeluar,koders,koderuangan,kodebed))
                    
                    return redirect('faskes:listReservasiFaskes')

                except: 
                    return render(request, 'reservasiRS.html', {'form': form})
                    form=ReservasiRS(initial=isi)
                # print('tes dokter 3')
                # form=CreateAppointment(initial=isi)
                # return render(request, 'form-appointment.html', 
                # {'form': form, 'message': 'Tidak bisa membuat perjanjian dengan dokter, silahkan pilih jadwal lainnya.'})
        
        form = ReservasiRS()
        with connection.cursor() as cursor:
            cursor.execute("SET search_path to SIRUCO;")
            cursor.execute("SELECT A.Kode_Faskes, B.KodeRuangan, C.KodeBed  FROM RUMAH_SAKIT A, RUANGAN_RS B, BED_RS C ")
            kodekodean = dictfecthall(cursor)

            hotelcode=set()
            

            for x in kodekodean:
                hotelcode.add(x['kode_faskes'])
            
            print ("Hotel code: ", hotelcode)
            temp = dict.fromkeys(hotelcode, [])
            
            print ("ISI TEMP:", temp)
            for k in temp:
                tempList = []    
                for x in kodekodean:
                        print ("X: ",x['kodehotel'])
                        print ("X: ",x['koderoom'])
                        print ("K: ",k)

                        if k == x['kodehotel']:
                            tempList.append(x['koderoom'])

                temp[k] = tempList
            

        pilihan = json.dumps(temp)

        
        args = {
            'pilihan' : pilihan,
            'user' : user_data,
            'form' : form,
        }
        print(args)
        print("di deket return")
        print (user_data)
        return render(request, 'form-reservasihotel.html', {'arg': args})


        return render(request, 'reservasi-rs.html', {'form': form})


#Navbar Reservasi RS ---------------------------------------------------------------------------------------------------
def listReservasiRS(request): 
    now = datetime.datetime.now()
    print(now.strftime("%d-%m-%Y"))

    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"] 
        role = request.session.get('role') 

        
        if role=="PENGGUNA_PUBLIK":
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS, SIRUCO.PASIEN  WHERE KodePasien=NIK AND IdPendaftar='{}';".format(username))
                list_reservasi = dictfecthall(cursor)
            return render(request, 'list-reservasi.html', {'listreservasi': list_reservasi,'hariini':now.strftime("%d-%m-%Y")})

        else :
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS;")
                list_reservasi = dictfecthall(cursor)
            return render(request, 'list-reservasi.html', {'listreservasi': list_reservasi,'hariini':now.strftime("%d-%m-%Y")})

def deleteReservasiRS(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]
    
        if request.method == 'GET':
            kodepasien = request.GET.get('kodepasien')
            kodebed = request.GET.get('kodebed')

        
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM SIRUCO.RESERVASI_RS WHERE KodePasien='{}' AND KodeBed='{}' ;".format(kodepasien,kodebed))
    
    return redirect('rumahsakit:listReservasiRS') 

def updateReservasiRS(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]

        if request.method == 'POST':

            nik = request.POST.get('nik')
            tanggal_masuk = request.POST.get('tanggal_masuk')
            tanggal_keluar = request.POST.get('tanggal_keluar')
            kode_rs = request.POST.get('kode_rs')
            kode_ruangan = request.POST.get('kode_ruangan')
            kode_bed = request.POST.get('kode_bed')

            isi = {'nik': nik, 
                'tanggal_masuk': tanggal_masuk, 
                'tanggal_keluar': tanggal_keluar,
                'kode_rs': kode_rs,
                'kode_ruangan': kode_ruangan,
                'kode_bed': kode_bed,
                }

            if tanggal_keluar > tanggal_masuk:
                with connection.cursor() as cursor:
                    cursor.execute("SET search_path to SIRUCO;")
                    cursor.execute(
                    """
                    UPDATE RESERVASI_RS SET TglKeluar= '{}'
                    WHERE kodepasien='{}';
                    """
                    .format(tanggal_keluar, nik))
                return redirect('rumahsakit:listReservasiRS')
            
            elif tanggal_keluar < tanggal_masuk:
                print ("ayolah tanggal jelek")
            # with connection.cursor() as cursor:
            #     cursor.execute("SET SEARCH_PATH TO SIRUCO;")
            #     cursor.execute("SELECT NoSTR FROM DOKTER where username=%s LIMIT 1;", [email])
            #     no_str = cursor.fetchone()[0]
                form=UpdateReservasiRS(initial=isi)
                return render(request, 'update-reservasi.html', 
                {'form': form, 'message': 'Tanggal keluar dibawah tanggal masuk'})
    
            #UPDATE TABLE MEMERIKSA 
            
        
        elif request.method == 'GET':
            nik = request.GET.get('kodepasien')
            tanggal_masuk = request.GET.get('tglmasuk')
            tanggal_keluar = request.GET.get('tglkeluar')
            kode_rs = request.GET.get('koders')
            kode_ruangan = request.GET.get('koderuangan')
            kode_bed = request.GET.get('kodebed')
            
            isi = {'nik': nik, 
                'tanggal_masuk': tanggal_masuk, 
                'tanggal_keluar': tanggal_keluar,
                'kode_rs': kode_rs,
                'kode_ruangan': kode_ruangan,
                'kode_bed': kode_bed,
                }

    

            # if isi is None:
            #     print("update isi none")
            #     return redirect('dokter:listAppointment')

            form=UpdateReservasiRS(initial=isi)
        return render(request, 'update-reservasi.html', {'form': form}) 

def transaksiRS(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"] 
        role = request.session.get('role') 

        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_RS")
            list_transaksi = dictfecthall(cursor)
            print (list_transaksi)
        
        return render(request, 'transaksi-rs.html', {'transaksi': list_transaksi})


def deleteTransaksi(request): 
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to SIRUCO;")
        username = request.session["username"]
    
        if request.method == 'GET':
            idtransaksi = request.GET.get('idtransaksi')

        
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM SIRUCO.TRANSAKSI_RS WHERE IdTransaksi='{}';".format(idtransaksi))
    
    return redirect('rumahsakit:transaksiRS')