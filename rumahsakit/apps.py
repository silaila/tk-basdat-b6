from django.apps import AppConfig


class RumahsakitConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rumahsakit'
