from django import forms
from django.forms import Form, ModelForm
from django.db import connection, transaction, IntegrityError


class DateInput (forms.DateInput):
    input_type = 'date'

class ReservasiRS(forms.Form):
    LIST_NIK = []
    LIST_RS = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK, NIK FROM SIRUCO.PASIEN")
            self.fields['nik'].choices = cursor.fetchall()

            cursor.execute("SELECT Kode_Faskes, Kode_Faskes FROM SIRUCO.RUMAH_SAKIT")
            self.fields['kode_rs'].choices = cursor.fetchall()

    nik = forms.ChoiceField(label='NIK Pasien',  choices= LIST_NIK, widget=forms.Select(attrs={'class': 'form-control'}))
    tanggal_masuk = forms.DateField(label="Tanggal Masuk", widget=DateInput)
    tanggal_keluar = forms.DateField(label="Tanggal Keluar", widget=DateInput)
    kode_rs = forms.ChoiceField(label="Kode Rumah Sakit", choices= LIST_RS, widget=forms.Select(attrs={'class': 'form-control'}))   
    # kode_ruangan = forms.ChoiceField(label="Kode Ruangan",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
    # kode_bed = forms.ChoiceField(label="Kode Bed",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))


class UpdateReservasiRS(forms.Form):
    nik = forms.CharField(label="NIK Pasien", required=False, widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    tanggal_masuk = forms.DateField(label="Tanggal Masuk", widget=DateInput(attrs={'readonly': True}))
    tanggal_keluar = forms.DateField(label="Tanggal Keluar", widget=DateInput)
    kode_rs = forms.CharField(label="Kode Rumah Sakit", widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
    kode_ruangan = forms.CharField(label="Kode Ruangan",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))   
    kode_bed = forms.CharField(label="Kode Bed",  widget=forms.TextInput(attrs={'class': 'form-control','readonly': True}))
    