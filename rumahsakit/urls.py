from django.urls import path, include
from . import views

app_name = 'rumahsakit'
urlpatterns = [ 
	path('reservasiRS/', views.reservasiRS, name='reservasiRS'),
    path('listReservasiRS/', views.listReservasiRS, name='listReservasiRS'),
    path('deleteReservasiRS/', views.deleteReservasiRS, name='deleteReservasiRS'),
    path('updateReservasiRS/', views.updateReservasiRS, name='updateReservasiRS'),
    path('transaksiRS/', views.transaksiRS, name='transaksiRS'),
    path('deleteTransaksi/', views.deleteTransaksi, name='deleteTransaksi'),
    # path('formAppointment/', views.formAppointment, name='formAppointment'),
    # path('listAppointment/', views.listAppointment, name='listAppointment'),
    # path('deleteAppointment/', views.deleteAppointment, name='deleteAppointment'),
    # path('updateAppointment/', views.updateAppointment, name='updateAppointment'),

]